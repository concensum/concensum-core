// Copyright (c) 2018 The Concensum Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <boost/test/unit_test.hpp>

#include <register/encoding.h>

#include <test/test_bitcoin.h>

/* No space between BOOST_FIXTURE_TEST_SUITE and '(', so that extraction of
   the test-suite name works with grep as done in the Makefile.  */
BOOST_AUTO_TEST_SUITE(register_test_encoding)

BOOST_AUTO_TEST_CASE (encoding_to_from_string)
{
  for (const std::string& encStr : {"ascii", "utf8", "hex"}) {
    BOOST_CHECK_EQUAL (EncodingToString (EncodingFromString (encStr)), encStr);
  }

  BOOST_CHECK_EQUAL (EncodingToString (RegisterEncoding::ASCII), "ascii");
  BOOST_CHECK_EQUAL (EncodingToString (RegisterEncoding::UTF8), "utf8");
  BOOST_CHECK_EQUAL (EncodingToString (RegisterEncoding::HEX), "hex");

  BOOST_CHECK_THROW (EncodingFromString ("invalid"), std::invalid_argument);
}

namespace
{

class EncodingTestSetup : public TestingSetup
{
public:

  RegisterEncoding encoding;

  void
  ValidRoundtrip (const std::string& str, const valtype& data) const
  {
    BOOST_CHECK_EQUAL (valtype2String(data, encoding), str);
    BOOST_CHECK (string2Valtype(str, encoding) == data);
  }

  void
  InvalidString (const std::string& str) const
  {
    BOOST_CHECK_THROW (string2Valtype(str, encoding), InvalidRegisterString);
  }

  void
  InvalidData (const valtype& data) const
  {
    BOOST_CHECK_THROW (valtype2String (data, encoding), InvalidRegisterString);
  }

};

} // anonymous namespace

BOOST_FIXTURE_TEST_CASE (encoding_ascii, EncodingTestSetup)
{
  encoding = RegisterEncoding::ASCII;

  ValidRoundtrip (" abc42\x7f", {0x20, 'a', 'b', 'c', '4', '2', 0x7f});
  ValidRoundtrip ("", {});

  InvalidString ("a\tx");
  InvalidString ("a\x80x");
  InvalidString (std::string ({'a', 0, 'x'}));
  InvalidString (u8"ä");

  InvalidData ({'a', 0, 'x'});
  InvalidData ({'a', 0x19, 'x'});
  InvalidData ({'a', 0x80, 'x'});
}

BOOST_FIXTURE_TEST_CASE (encoding_utf8, EncodingTestSetup)
{
  encoding = RegisterEncoding::UTF8;

  valtype expected({0x20, 'a', 'b', 'c', '\t', '4', '2', 0x00, 0x7f});
  const std::string utf8Str = u8"äöü";
  BOOST_CHECK_EQUAL (utf8Str.size (), 6);
  expected.insert (expected.end (), utf8Str.begin (), utf8Str.end ());
  ValidRoundtrip (" abc\t42" + std::string ({0}) + u8"\x7fäöü", expected);
  ValidRoundtrip ("", {});

  InvalidString ("a\x80x");
  InvalidData ({'a', 0x80, 'x'});
}

BOOST_FIXTURE_TEST_CASE (encoding_hex, EncodingTestSetup)
{
  encoding = RegisterEncoding::HEX;

  ValidRoundtrip ("0065ff", {0x00, 0x65, 0xff});
  ValidRoundtrip ("", {});
  BOOST_CHECK (string2Valtype ("aaBBcCDd", encoding)
               == valtype ({0xaa, 0xbb, 0xcc, 0xdd}));

  ValidRoundtrip("fedca102", {0xfe, 0xdc, 0xa1, 0x02});

  InvalidString ("aaa");
  InvalidString ("zz");
}

BOOST_AUTO_TEST_SUITE_END ()
