// Copyright (c) 2018 The Concensum Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <boost/test/execution_monitor.hpp>
#include <boost/test/unit_test.hpp>
#include <test/test_bitcoin.h>

#include <base58.h>
#include <coins.h>
#include <consensus/validation.h>
#include <key_io.h>
#include <keystore.h>
#include <register/encoding.h>
#include <register/main.h>
#include <script/ismine.h>
#include <script/register.h>
#include <script/standard.h>
#include <validation.h>
#include <policy/policy.h>

#include <wallet/test/wallet_test_fixture.h>

/* No space between BOOST_FIXTURE_TEST_SUITE and '(', so that extraction of
   the test-suite name works with grep as done in the Makefile.  */
BOOST_FIXTURE_TEST_SUITE(register_test_script, TestingSetup)

namespace
{
/**
 * Utility function that returns a sample address script to use in the tests.
 * @return A script that represents a simple address.
 */
CScript
getTestAddress(int n = 0)
{
  std::string addresses[] = {
    "CXFpsAu4F3WYFAKTGZrnMNsFKVa2ThkFPF",
    "CLPw7DfcU4CtGNwFuuEEHyVKQvWx8iMNeA",
    "CKzVEEKSMjN6KJ8CX62sKbLBeZJFi1ibcJ"
  };

  const CTxDestination dest = DecodeDestination(addresses[n]);
  BOOST_CHECK (IsValidDestination(dest));

  return GetScriptForDestination(dest);
}

} // anonymous namespace

BOOST_AUTO_TEST_CASE (test_sanity)
{
  const CScript addr = getTestAddress ();
  const CRegisterScript opNone(addr);
  BOOST_CHECK (!opNone.isRegisterOp());
  BOOST_CHECK (opNone.getAddress() == addr);

  const valtype someId = string2Valtype("i-am-a-record-id");
  const valtype someValue = string2Valtype("{\"v\": \"0\"}");

  CScript script = CRegisterScript::buildStoreRecord(addr, someId, someValue);
  const CRegisterScript opRegister(script);

  BOOST_CHECK (opRegister.isRegisterOp());
  BOOST_CHECK (opRegister.getAddress() == addr);
  BOOST_CHECK (opRegister.getId() == someId);
  BOOST_CHECK (opRegister.getValue() == someValue);
}

BOOST_AUTO_TEST_CASE (register_database)
{
  const valtype registrar = string2Valtype("my-registrar");
  const valtype id = string2Valtype("my-id");
  const valtype value = string2Valtype("my-value"); // TODO 2 different values for..
  const CScript addr = getTestAddress();

  CRegisterData valueRet, valueSample1, valueSample2;
  CScript script = CRegisterScript::buildStoreRecord(addr, id, value);
  const CRegisterScript registerOp(script);
  valueSample1.fromScript(COutPoint(uint256(), 1), registerOp);
  valueSample2.fromScript(COutPoint(uint256(), 3), registerOp);

  CCoinsViewCache& cache = *pcoinsTip;

  // no record found
  BOOST_CHECK(!cache.GetRecord(registrar, id, valueRet));

  // set record, get same value
  cache.SetRecord(registrar, id, valueSample1);
  BOOST_CHECK(cache.GetRecord(registrar, id, valueRet));
  BOOST_CHECK(valueSample1 == valueRet);

  // flush cache, get record anyway
  BOOST_CHECK(cache.Flush());
  BOOST_CHECK(cache.GetRecord(registrar, id, valueRet));
  BOOST_CHECK(valueSample1 == valueRet);

  // override record, get new value
  cache.SetRecord(registrar, id, valueSample2);
  BOOST_CHECK(cache.GetRecord(registrar, id, valueRet));
  BOOST_CHECK(valueSample2 == valueRet);
  // sanity check
  BOOST_CHECK(valueSample2 != valueSample1);

  // delete record, record not found
  cache.DeleteRecord(registrar, id);
  BOOST_CHECK(!cache.GetRecord(registrar, id, valueRet));

  // flush after delete, still not found
  BOOST_CHECK(cache.Flush());
  BOOST_CHECK(!cache.GetRecord(registrar, id, valueRet));
}

BOOST_AUTO_TEST_CASE(register_pubkeyhash_script)
{
  const valtype id = string2Valtype("some-id");
  const valtype value = string2Valtype("some-value");
  const CScript addr = getTestAddress();

  CScript registerScript = CRegisterScript::buildStoreRecord(addr, id, value);

  txnouttype typeRet;
  std::vector<std::vector<unsigned char>> vSolutionsRet;
  bool isValid = Solver(registerScript, typeRet, vSolutionsRet);

  BOOST_CHECK(isValid);
  BOOST_CHECK(TX_REGISTER == typeRet);

  // make sure vSolutionsRet is being set as expected
  CTxDestination pubkeyhashDest;
  ExtractDestination(addr, pubkeyhashDest);
  BOOST_CHECK(pubkeyhashDest == (CTxDestination)CKeyID(uint160(vSolutionsRet[0])));
  BOOST_CHECK(vSolutionsRet.size() == 1);

  // test ExtractDestination for register_pubkeyhash
  CTxDestination register_pubkeyhashDest;
  ExtractDestination(registerScript, register_pubkeyhashDest);
  BOOST_CHECK(register_pubkeyhashDest == pubkeyhashDest);
}

BOOST_AUTO_TEST_CASE(script_register_IsMine)
{
  CKey keys[2];
  CPubKey pubkeys[2];
  for (int i = 0; i < 2; i++) {
      keys[i].MakeNewKey(true);
      pubkeys[i] = keys[i].GetPubKey();
  }

  CKey uncompressedKey;
  uncompressedKey.MakeNewKey(false);
  CPubKey uncompressedPubkey = uncompressedKey.GetPubKey();

  CScript scriptPubKey;
  isminetype result;

  const valtype someId = string2Valtype("someId");
  const valtype someValue = string2Valtype("someValue");

  // R2PKH compressed
  {
    CBasicKeyStore keystore;
    CScript pubkeyhash = GetScriptForDestination(pubkeys[0].GetID());
    scriptPubKey = CRegisterScript::buildStoreRecord(pubkeyhash, someId, someValue);

    // Keystore does not have key
    result = IsMine(keystore, scriptPubKey);
    BOOST_CHECK_EQUAL(result, ISMINE_NO);

    // Keystore has key
    keystore.AddKey(keys[0]);
    result = IsMine(keystore, scriptPubKey);
    BOOST_CHECK_EQUAL(result, ISMINE_SPENDABLE);
  }

  // R2PKH uncompressed
  {
    CBasicKeyStore keystore;
    CScript pubkeyhash = GetScriptForDestination(uncompressedPubkey.GetID());
    scriptPubKey = CRegisterScript::buildStoreRecord(pubkeyhash, someId, someValue);

    // Keystore does not have key
    result = IsMine(keystore, scriptPubKey);
    BOOST_CHECK_EQUAL(result, ISMINE_NO);

    // Keystore has key
    keystore.AddKey(uncompressedKey);
    result = IsMine(keystore, scriptPubKey);
    BOOST_CHECK_EQUAL(result, ISMINE_SPENDABLE);
  }
}

BOOST_FIXTURE_TEST_CASE(script_register_IsSolvable, WalletTestingSetup)
{
  const valtype someId = string2Valtype("someId");
  const valtype someValue = string2Valtype("someValue");

  CPubKey pubkey;
  BOOST_CHECK(m_wallet.GetKeyFromPool(pubkey, false));

  CScript pubkeyhash = GetScriptForDestination(pubkey.GetID());
  CScript registerScript = CRegisterScript::buildStoreRecord(pubkeyhash, someId, someValue);

  txnouttype typeRet;
  std::vector<std::vector<unsigned char>> vSolutionsRet;
  bool isValid = Solver(registerScript, typeRet, vSolutionsRet);

  BOOST_CHECK(isValid);
  BOOST_CHECK(TX_REGISTER == typeRet);

  BOOST_CHECK(IsSolvable(m_wallet, registerScript));
}

/* ************************************************************************** */

/**
 * Construct a dummy tx that provides the given script as input
 * for further tests in the given CCoinsView.  The txid is returned
 * to refer to it.  The "index" is always 0.
 * @param scr The script that should be provided as output.
 * @param nHeight The height of the coin.
 * @param view Add it to this view.
 * @return The out point for the newly added coin.
 */
static COutPoint
addTestCoin(const CScript& scr, unsigned nHeight, unsigned nAmount, CCoinsViewCache& view)
{
  const CTxOut txout(nAmount * COIN, scr);

  CMutableTransaction mtx;
  mtx.vout.push_back(txout);
  const CTransaction tx(mtx);

  Coin coin(txout, nHeight, false, false);
  const COutPoint outp(tx.GetHash(), 0);
  view.AddCoin(outp, std::move(coin), false);

  return outp;
}

BOOST_AUTO_TEST_CASE(check_register_transaction)
{
  const CScript addr0 = getTestAddress(0);
  const CScript addr1 = getTestAddress(1);
  const CScript addr2 = getTestAddress(2);

  CCoinsView dummyView;
  CCoinsViewCache view(&dummyView);

  const COutPoint in100CoinFromAddr0 = addTestCoin(addr0, 1, 100, view);
  const COutPoint in10KCoinFromAddr0 = addTestCoin(addr0, 1, 10000, view);
  const COutPoint in10KCoinFromAddr2 = addTestCoin(addr2, 1, 10000, view);

  CValidationState state;
  CMutableTransaction mtx0;
  CScript src;
  std::string reason;

  mtx0.vin.push_back(CTxIn(in100CoinFromAddr0));
  mtx0.vout.push_back(CTxOut(COIN, addr0));
  const CTransaction baseTx(mtx0);

  // no register input or outputs, should PASS
  BOOST_CHECK(CheckRegisterTransaction(baseTx, view, state));

  // register ouput, sum of inputs < 10k, should FAIL
  mtx0 = CMutableTransaction(baseTx);
  const valtype id0 = string2Valtype("id0");
  const valtype value0 = string2Valtype("value0");
  const CScript scriptStoreToAddr0 = CRegisterScript::buildStoreRecord(addr0, id0, value0);
  mtx0.vout.push_back(CTxOut(0, scriptStoreToAddr0));
  BOOST_CHECK(!CheckRegisterTransaction(mtx0, view, state));

  // register ouput, sum of inputs >= 10k, should PASS
  mtx0.vin.push_back(CTxIn(in10KCoinFromAddr2));
  BOOST_CHECK(CheckRegisterTransaction(mtx0, view, state));

  // register destination addr is not part of input addresses, should FAIL
  CMutableTransaction mtx1;
  mtx1.vin.push_back(CTxIn(in10KCoinFromAddr2));
  mtx1.vout.push_back(CTxOut(0, scriptStoreToAddr0));
  BOOST_CHECK(!CheckRegisterTransaction(mtx1, view, state));

  // register destination is part of input address, should PASS
  mtx1.vin.push_back(CTxIn(in100CoinFromAddr0));
  BOOST_CHECK(CheckRegisterTransaction(mtx1, view, state));

  const CTransaction baseValidRegisterTx(mtx0);

  // different-address, same-id, same-value should FAIL
  CMutableTransaction mtx2 = CMutableTransaction(baseValidRegisterTx);
  const CScript scriptStoreId0ToAddr1 = CRegisterScript::buildStoreRecord(addr1, id0, value0);
  const COutPoint inStoreId0FromAddr1 = addTestCoin(scriptStoreId0ToAddr1, 1, 0, view);
  mtx2.vin.push_back(CTxIn(inStoreId0FromAddr1));
  BOOST_CHECK(!CheckRegisterTransaction(mtx2, view, state));

  // same-address, different-id, should FAIL
  CMutableTransaction mtx25 = CMutableTransaction(baseValidRegisterTx);
  const valtype id1 = string2Valtype("id1");
  const valtype value1 = string2Valtype("value1");
  const CScript scriptStoreId1ToAddr0WValue1 = CRegisterScript::buildStoreRecord(addr0, id1, value1);
  const COutPoint inStoreId1FromAddr0WValue1 = addTestCoin(scriptStoreId1ToAddr0WValue1, 1, 0, view);
  mtx25.vin.push_back(CTxIn(inStoreId1FromAddr0WValue1));
  BOOST_CHECK(!CheckRegisterTransaction(mtx25, view, state));

  // same-address, same-id, different-value should PASS
  CMutableTransaction mtx3 = CMutableTransaction(baseValidRegisterTx);
  const CScript scriptStoreId0ToAddr0WValue1 = CRegisterScript::buildStoreRecord(addr0, id0, value1);
  const COutPoint inStoreId0FromAddr0WValue1 = addTestCoin(scriptStoreId0ToAddr0WValue1, 1, 0, view);
  mtx3.vin.push_back(CTxIn(inStoreId0FromAddr0WValue1));
  BOOST_CHECK(CheckRegisterTransaction(mtx3, view, state));

  // 2 register inputs should FAIL
  mtx3.vin.push_back(CTxIn(inStoreId1FromAddr0WValue1));
  BOOST_CHECK(!CheckRegisterTransaction(mtx3, view, state));

  // 1 register input, 2 register outputs should FAIL
  CMutableTransaction mtx4 = CMutableTransaction(baseValidRegisterTx);
  mtx4.vin.push_back(CTxIn(inStoreId1FromAddr0WValue1));
  mtx4.vout.push_back(CTxOut(0, scriptStoreId1ToAddr0WValue1));
  BOOST_CHECK(!CheckRegisterTransaction(mtx4, view, state));

  // 1 register input, no register output should FAIL
  // CMutableTransaction mtx5;
  // mtx5.vin.push_back(CTxIn(in10KCoinFromAddr0));
  // mtx5.vin.push_back(CTxIn(inStoreId1FromAddr0WValue1));
  // mtx5.vout.push_back(CTxOut(COIN, addr0));
  // BOOST_CHECK(!CheckRegisterTransaction(mtx5, view, state));

  // too long id, should FAIL!!
  CMutableTransaction mtx6;
  mtx6.vin.push_back(CTxIn(in10KCoinFromAddr0));
  const valtype tooLongId(256, 'x');
  const CScript scriptStoreTooLongIdToAddr0WValue1 = CRegisterScript::buildStoreRecord(addr0, tooLongId, value1);
  mtx6.vout.push_back(CTxOut(0, scriptStoreTooLongIdToAddr0WValue1));
  BOOST_CHECK(!CheckRegisterTransaction(mtx6, view, state));

  // too long value, should FAIL!!
  CMutableTransaction mtx7;
  mtx7.vin.push_back(CTxIn(in100CoinFromAddr0));
  const valtype tooLongValue(1024, 'x');
  const CScript scriptStoreId0ToAddr0WTooLongValue = CRegisterScript::buildStoreRecord(addr0, id0, tooLongValue);
  mtx7.vout.push_back(CTxOut(0, scriptStoreId0ToAddr0WTooLongValue));
  BOOST_CHECK(!CheckRegisterTransaction(mtx7, view, state));

  // TODO check state'??
}

BOOST_AUTO_TEST_CASE(register_regression_CheckRegisterTransaction)
{
  /**
   * for non-register tx with a non solvabe/no destination script, 
   * checkRegisterTransaction should not reject the transaction
   */

  CCoinsView dummyView;
  CCoinsViewCache view(&dummyView);

  CScript nonStandardScript = CScript();
  const COutPoint inNonStandardScript = addTestCoin(nonStandardScript, 1, 10, view);

  CMutableTransaction mtx0;
  mtx0.vin.push_back(CTxIn(inNonStandardScript));
  mtx0.vout.push_back(CTxOut(COIN, getTestAddress(0)));
  CTransaction tx0(mtx0);
  
  // assert its not register tx
  BOOST_CHECK(!tx0.HasOpRegisterOut());

  // assert it has no destination
  CTxDestination dest;
  BOOST_CHECK(!ExtractDestination(nonStandardScript, dest));

  // does not fail
  CValidationState state;
  BOOST_CHECK(CheckRegisterTransaction(tx0, view, state));

}

BOOST_AUTO_TEST_CASE(register_mempool)
{
  LOCK(mempool.cs);
  mempool.clear();

  TestMemPoolEntryHelper entry;
  const CScript addr = getTestAddress();
  const CScript addr1 = getTestAddress(1);
  const CScript addr2 = getTestAddress(2);
  const valtype id = string2Valtype("some-id");
  const valtype id1 = string2Valtype("some-id-1");
  const valtype value = string2Valtype("some-value");

  CCoinsViewCache view(pcoinsTip.get());
  const COutPoint in10KCoinFromAddr = addTestCoin(addr, 1, 10000, view);
  const COutPoint in20KCoinFromAddr = addTestCoin(addr, 1, 20000, view);
  const COutPoint in10KCoinFromAddr1 = addTestCoin(addr1, 1, 10000, view);

  const CScript record = CRegisterScript::buildStoreRecord(addr, id, value);
  const CScript record1 = CRegisterScript::buildStoreRecord(addr1, id, value);
  const CScript record2 = CRegisterScript::buildStoreRecord(addr2, id, value);
  const CScript record3 = CRegisterScript::buildStoreRecord(addr, id1, value);

  CMutableTransaction mtx0;
  CMutableTransaction mtx1;
  CMutableTransaction mtx2;
  CMutableTransaction mtx3;

  CValidationState state;

  CTxIn in10kAddr = CTxIn(in10KCoinFromAddr);
  CTxIn in20kAddr = CTxIn(in20KCoinFromAddr);
  CTxIn in10kAddr1 = CTxIn(in10KCoinFromAddr1);
  mtx0.vin.push_back(in10kAddr);
  mtx0.vout.push_back(CTxOut(0, record));
  mtx0.vout.push_back(CTxOut(COIN, addr));

  mtx1.vin.push_back(in10kAddr1);
  mtx1.vout.push_back(CTxOut(0, record1));
  mtx1.vout.push_back(CTxOut(COIN, addr1));

  mtx2.vin.push_back(in10kAddr);
  mtx2.vout.push_back(CTxOut(0, record2));
  mtx2.vout.push_back(CTxOut(COIN, addr2));

  mtx3.vin.push_back(in20kAddr);
  mtx3.vout.push_back(CTxOut(0, record3));
  mtx3.vout.push_back(CTxOut(COIN, addr));

  //store record transaction
  const CMutableTransaction recordTransaction(mtx0);
  const CMutableTransaction recordTransaction1(mtx1);
  const CMutableTransaction recordTransaction2(mtx2);
  const CMutableTransaction recordTransaction3(mtx3);

  //should PASS
  BOOST_CHECK(CheckRegisterTransaction(mtx0, view, state));
  BOOST_CHECK(CheckRegisterTransaction(mtx1, view, state));

  CTxDestination dst;
  ExtractDestination(addr, dst);
  const valtype valAddr = destination2Valtype(dst);

  // not yet added to the mempool
  BOOST_CHECK(!mempool.isStoreRecordInMempool(valAddr, id));

  CTxDestination dst1;
  ExtractDestination(addr1, dst1);
  const valtype valAddr1 = destination2Valtype(dst1);

  // not yet added to the mempool
  BOOST_CHECK(!mempool.isStoreRecordInMempool(valAddr1, id));

  //check it can be added to mempool
  BOOST_CHECK(mempool.checkRegisterOp(recordTransaction));
  BOOST_CHECK(mempool.checkRegisterOp(recordTransaction1));

  const LockPoints lp;
  const CTxMemPoolEntry recordEntry(MakeTransactionRef(recordTransaction), 0, 0, 100, false, 1, lp);
  const CTxMemPoolEntry recordEntry1(MakeTransactionRef(recordTransaction1), 0, 0, 100, false, 1, lp);
  const CTxMemPoolEntry recordEntry2(MakeTransactionRef(recordTransaction2), 0, 0, 100, false, 1, lp);
  const CTxMemPoolEntry recordEntry3(MakeTransactionRef(recordTransaction3), 0, 0, 100, false, 1, lp);

  mempool.addUnchecked(recordEntry.GetTx().GetHash(), recordEntry);

  //can't add already added transaction
  BOOST_CHECK(!mempool.checkRegisterOp(recordTransaction));

  // added to the mempool
  BOOST_CHECK(mempool.isStoreRecordInMempool(valAddr, id));

  //record1 is not in mempool
  BOOST_CHECK(!mempool.isStoreRecordInMempool(valAddr1, id));

  //record 1 can be added to mempool
  BOOST_CHECK(mempool.checkRegisterOp(recordTransaction1));
  mempool.addUnchecked(recordEntry1.GetTx().GetHash(), recordEntry1);

  //remove from mempool a conflicted transaction
  mempool.removeConflicts(recordTransaction2);

  //record is not in mempool
  BOOST_CHECK(!mempool.isStoreRecordInMempool(valAddr, id));

  // mempool sanity check
  const CRegisterScript regScript(record);
  CRegisterData data;
  data.fromScript(COutPoint(uint256(), 0), regScript);
  view.SetRecord(valAddr, id, data);
  mempool.checkRegisters(&view);

  mempool.removeRecursive(recordTransaction1);

  //check record1 was removed from mempool
  BOOST_CHECK(!mempool.isStoreRecordInMempool(valAddr1, id));

  BOOST_CHECK_EQUAL(mempool.size(), 0);

  // add 3 entries, two on one address and the remaining on another address
  //same address
  mempool.addUnchecked(recordEntry.GetTx().GetHash(), recordEntry);
  mempool.addUnchecked(recordEntry3.GetTx().GetHash(), recordEntry3);
  //different address
  mempool.addUnchecked(recordEntry1.GetTx().GetHash(), recordEntry1);

  //should not break test on assertion
  mempool.checkRegisters(&view);

}

BOOST_AUTO_TEST_CASE(register_is_dust) {


  const CScript payToPubkeyHash = getTestAddress(0);

  static CFeeRate minFeeRate = CFeeRate(DEFAULT_BLOCK_MIN_TX_FEE);

  static CFeeRate tenTimesMinFeeRate = CFeeRate(10 * DEFAULT_BLOCK_MIN_TX_FEE);

  // with fee rate 0, pay_pubkeyhash with nAmount = 0 IS NOT DUST
  BOOST_CHECK(!IsDust(CTxOut(0, payToPubkeyHash), CFeeRate(0)));

  // with fee rate set to min, pay_pubkeyhash with nAmount = 0 IS DUST
  BOOST_CHECK(IsDust(CTxOut(0, payToPubkeyHash), minFeeRate));

  // with fee rate set to min, pay_pubkeyhash with nAmount set IS NOT DUST
  BOOST_CHECK(!IsDust(CTxOut(COIN, payToPubkeyHash), minFeeRate));

  const CScript registerToPubkeyHash = CRegisterScript::buildStoreRecord(
    getTestAddress(0)
    , string2Valtype("id")
    , string2Valtype("value"));

  // with fee rate set, register_pubkeyhash with nAmount 0 IS NOT DUST
  BOOST_CHECK(!IsDust(CTxOut(0, registerToPubkeyHash), minFeeRate));

  // with fee rate higher, register_pubkeyhash with nAmount 0 IS NOT DUST
  BOOST_CHECK(!IsDust(CTxOut(0, registerToPubkeyHash), tenTimesMinFeeRate));
}

BOOST_AUTO_TEST_SUITE_END()
