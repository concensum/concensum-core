// Copyright (c) 2018 The Concensum Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <boost/test/unit_test.hpp>

#include <key_io.h>
#include <coins.h>
#include <undo.h>
// #include <consensus/validation.h>
// #include <script/standard.h>

#include <register/main.h>
#include <register/encoding.h>
#include <script/register.h>

#include <test/test_bitcoin.h>

/* No space between BOOST_FIXTURE_TEST_SUITE and '(', so that extraction of
   the test-suite name works with grep as done in the Makefile.  */
BOOST_FIXTURE_TEST_SUITE(register_test_apply_undo, TestingSetup)

namespace
{
/**
 * Utility function that returns a sample address script to use in the tests.
 * @return A script that represents a simple address.
 */

// TODO move this to common fixture..

CTxDestination
getTestDestination(int n = 0)
{
  std::string addresses[] = {
    "CXFpsAu4F3WYFAKTGZrnMNsFKVa2ThkFPF",
    "CLPw7DfcU4CtGNwFuuEEHyVKQvWx8iMNeA",
    "CKzVEEKSMjN6KJ8CX62sKbLBeZJFi1ibcJ"
  };

  const CTxDestination dest = DecodeDestination(addresses[n]);
  BOOST_CHECK (IsValidDestination (dest));

  return dest;
}

CScript
getTestAddress(int n = 0)
{
  const CTxDestination dest = getTestDestination(n);
  return GetScriptForDestination(dest);
}

static COutPoint
addTestCoin (const CScript& scr, unsigned nHeight, unsigned nAmount, CCoinsViewCache& view)
{
  const CTxOut txout(nAmount * COIN, scr);

  CMutableTransaction mtx;
  mtx.vout.push_back (txout);
  const CTransaction tx(mtx);

  Coin coin(txout, nHeight, false, false);
  const COutPoint outp(tx.GetHash (), 0);
  view.AddCoin (outp, std::move (coin), false);

  return outp;
}

} // anonymous namespace

BOOST_AUTO_TEST_CASE (register_store_undo)
{
  CCoinsView dummyView;
  CCoinsViewCache view(&dummyView);

  CBlockUndo undo;
  CRegisterData data;

  const CTxDestination dest0 = getTestDestination(0);
  const CScript addr0 = GetScriptForDestination(dest0);
  const valtype registrar0 = destination2Valtype(dest0);
  const valtype id0 = string2Valtype("id-0");
  const valtype id1 = string2Valtype("id-1");
  const valtype value0 = string2Valtype("value-0");
  const valtype value1 = string2Valtype("value-1");

  const CScript storeScript0 = CRegisterScript::buildStoreRecord(addr0, id0, value0);
  const CScript storeScript1 = CRegisterScript::buildStoreRecord(addr0, id1, value1);

  CMutableTransaction mtx;
  mtx.vout.push_back(CTxOut(COIN, addr0));
  // apply tx without register output
  ApplyRegisterTransaction(CTransaction(mtx), 100, view, undo);
  // record does not exist yet
  BOOST_CHECK(!view.GetRecord(registrar0, id0, data));
  // vregisterundo is empty
  BOOST_CHECK(undo.vregisterundo.empty());

  // add register output
  mtx.vout.push_back(CTxOut(0, storeScript0));
  // apply it
  ApplyRegisterTransaction(CTransaction(mtx), 200, view, undo);
  // check it was persisted
  BOOST_CHECK(view.GetRecord(registrar0, id0, data));
  BOOST_CHECK(data.getValue() == value0);
  BOOST_CHECK(data.getAddress() == addr0);
  // check it was added to undos
  BOOST_CHECK(undo.vregisterundo.size() == 1);


  // new tx with another register output
  mtx.vout.clear();
  mtx.vout.push_back(CTxOut(0, storeScript1));
  // not there yet
  BOOST_CHECK(!view.GetRecord(registrar0, id1, data));
  // apply it
  ApplyRegisterTransaction(CTransaction(mtx), 200, view, undo);
  // check it was persisted
  BOOST_CHECK(view.GetRecord(registrar0, id1, data));
  BOOST_CHECK(data.getValue() == value1);
  BOOST_CHECK(data.getAddress() == addr0);
  // check it was added to undos
  BOOST_CHECK(undo.vregisterundo.size() == 2);

  // undo last operation
  undo.vregisterundo.back().apply(view);
  // id1 not found
  BOOST_CHECK(!view.GetRecord(registrar0, id1, data));
  // id0 kept the same
  BOOST_CHECK(view.GetRecord(registrar0, id0, data));
  BOOST_CHECK(data.getValue() == value0);
  BOOST_CHECK(data.getAddress() == addr0);
  undo.vregisterundo.pop_back();

  // undo again
  undo.vregisterundo.back().apply(view);
  // id0 not found
  BOOST_CHECK(!view.GetRecord(registrar0, id0, data));
  undo.vregisterundo.pop_back();

  BOOST_CHECK(undo.vregisterundo.empty());
}

BOOST_AUTO_TEST_CASE (register_update_undo)
{
  CCoinsView dummyView;
  CCoinsViewCache view(&dummyView);

  CBlockUndo undo;
  CRegisterData data;

  const CTxDestination dest0 = getTestDestination(0);
  const CScript addr0 = GetScriptForDestination(dest0);
  const valtype registrar0 = destination2Valtype(dest0);
  const valtype id0 = string2Valtype("id-0");
  const valtype initialValue = string2Valtype("value-0");
  const valtype updatedValue = string2Valtype("value-1");

  const CScript storeScript0 = CRegisterScript::buildStoreRecord(addr0, id0, initialValue);
  const CScript updateScript0 = CRegisterScript::buildStoreRecord(addr0, id0, updatedValue);

  CMutableTransaction mtx;

  BOOST_CHECK(undo.vregisterundo.empty());

  // add register output
  mtx.vout.push_back(CTxOut(0, storeScript0));
  // apply it
  const CTransaction storeTx = CTransaction(mtx);
  ApplyRegisterTransaction(storeTx, 200, view, undo);
  // check it was persisted
  BOOST_CHECK(view.GetRecord(registrar0, id0, data));
  BOOST_CHECK(data.getValue() == initialValue);
  BOOST_CHECK(data.getAddress() == addr0);
  // check it was added to undos
  BOOST_CHECK(undo.vregisterundo.size() == 1);

  // create pseudo update tx
  mtx.vout.clear();
  const COutPoint inStoreCoin = addTestCoin(storeScript0, 1, 0, view);
  mtx.vin.push_back(CTxIn(inStoreCoin));
  mtx.vout.push_back(CTxOut(0, updateScript0));
  // apply it
  const CTransaction updateTx = CTransaction(mtx);
  ApplyRegisterTransaction(updateTx, 200, view, undo);
  // get updated data
  BOOST_CHECK(view.GetRecord(registrar0, id0, data));
  // check it was updated
  BOOST_CHECK(data.getValue() == updatedValue);
  BOOST_CHECK(data.getAddress() == addr0);
  // check it was added to undos
  BOOST_CHECK(undo.vregisterundo.size() == 2);

  // undo last operation
  undo.vregisterundo.back().apply(view);
  // get data
  BOOST_CHECK(view.GetRecord(registrar0, id0, data));
  // value was reseted
  BOOST_CHECK(data.getValue() == initialValue);
  BOOST_CHECK(data.getAddress() == addr0);
  undo.vregisterundo.pop_back();

  // undo again
  undo.vregisterundo.back().apply(view);
  // id0 not found
  BOOST_CHECK(!view.GetRecord(registrar0, id0, data));
  undo.vregisterundo.pop_back();

  BOOST_CHECK(undo.vregisterundo.empty());
}

BOOST_AUTO_TEST_CASE (register_delete_undo)
{
  CCoinsView dummyView;
  CCoinsViewCache view(&dummyView);

  CBlockUndo undo;
  CRegisterData data;

  const CTxDestination dest0 = getTestDestination(0);
  const CScript addr0 = GetScriptForDestination(dest0);
  const valtype registrar0 = destination2Valtype(dest0);
  const valtype id0 = string2Valtype("id-0");
  const valtype value0 = string2Valtype("value-0");

  const CScript storeScript0 = CRegisterScript::buildStoreRecord(addr0, id0, value0);

  CMutableTransaction mtx;

  BOOST_CHECK(undo.vregisterundo.empty());

  // Apply store register
  mtx.vout.push_back(CTxOut(0, storeScript0));
  const CTransaction storeTx = CTransaction(mtx);
  ApplyRegisterTransaction(storeTx, 200, view, undo);
  // Check it is there
  BOOST_CHECK(view.GetRecord(registrar0, id0, data));
  BOOST_CHECK(data.getValue() == value0);
  BOOST_CHECK(data.getAddress() == addr0);
  BOOST_CHECK(undo.vregisterundo.size() == 1);

  // Apply delete register
  mtx.vout.clear();
  const COutPoint inStoreCoin = addTestCoin(storeScript0, 1, 0, view);
  mtx.vin.push_back(CTxIn(inStoreCoin));
  const CTransaction deleteTx = CTransaction(mtx);
  ApplyRegisterTransaction(deleteTx, 200, view, undo);
  // Check it was deleted
  BOOST_CHECK(!view.GetRecord(registrar0, id0, data));
  BOOST_CHECK(undo.vregisterundo.size() == 2);

  // Undo delete, so it is back there
  undo.vregisterundo.back().apply(view);
  BOOST_CHECK(view.GetRecord(registrar0, id0, data));
  BOOST_CHECK(data.getValue() == value0);
  BOOST_CHECK(data.getAddress() == addr0);
  undo.vregisterundo.pop_back();

  // Undo store, puff
  undo.vregisterundo.back().apply(view);
  BOOST_CHECK(!view.GetRecord(registrar0, id0, data));
  undo.vregisterundo.pop_back();

  BOOST_CHECK(undo.vregisterundo.empty());
}

BOOST_AUTO_TEST_SUITE_END ()
