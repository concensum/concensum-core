// Copyright (c) 2019 The Concensum Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <boost/test/unit_test.hpp>

#include <wallet/test/wallet_test_fixture.h>

#include <script/register.h>
#include <register/encoding.h>

#include <wallet/wallet.h>
#include <wallet/coincontrol.h>


BOOST_AUTO_TEST_SUITE(register_test_wallet)

namespace
{

static void AddKey(CWallet& wallet, const CKey& key)
{
    LOCK(wallet.cs_wallet);
    wallet.AddKeyPubKey(key, key.GetPubKey());
}

class RegisterOutputsAreNotForStakingSetup : public TestChain100Setup
{
public:
    RegisterOutputsAreNotForStakingSetup()
    {
        CreateAndProcessBlock({}, GetScriptForRawPubKey(coinbaseKey.GetPubKey()));
        wallet = MakeUnique<CWallet>("mock", WalletDatabase::CreateMock());
        bool firstRun;
        wallet->LoadWallet(firstRun);
        AddKey(*wallet, coinbaseKey);
        WalletRescanReserver reserver(wallet.get());
        reserver.reserve();
        wallet->ScanForWalletTransactions(chainActive.Genesis(), nullptr, reserver);

        // following blocks will be mined to a non-wallet addr
        coinbase2Key.MakeNewKey(true);
    }

    ~RegisterOutputsAreNotForStakingSetup()
    {
        wallet.reset();
    }

    CWalletTx& AddTx(const std::vector<CRecipient>& vecSend)
    {
        CTransactionRef tx;
        CReserveKey reservekey(wallet.get());
        CAmount fee;
        int changePos = -1;
        std::string error;
        CCoinControl dummy;
        BOOST_CHECK(wallet->CreateTransaction(vecSend, tx, reservekey, fee, changePos, error, dummy));
        CValidationState state;
        BOOST_CHECK(wallet->CommitTransaction(tx, {}, {}, {}, reservekey, nullptr, state));
        CMutableTransaction blocktx;
        {
            LOCK(wallet->cs_wallet);
            blocktx = CMutableTransaction(*wallet->mapWallet.at(tx->GetHash()).tx);
        }
        CreateAndProcessBlock({CMutableTransaction(blocktx)}, GetScriptForRawPubKey(coinbase2Key.GetPubKey()));
        LOCK(wallet->cs_wallet);
        auto it = wallet->mapWallet.find(tx->GetHash());
        BOOST_CHECK(it != wallet->mapWallet.end());
        it->second.SetMerkleBranch(chainActive.Tip(), 1);
        return it->second;
    }

    void GenerateEmptyBlock(int n)
    {
      for (int i = 0; i < COINBASE_MATURITY; i++)
      {
        std::vector<CMutableTransaction> noTxns;
        CBlock b = CreateAndProcessBlock(noTxns, GetScriptForRawPubKey(coinbase2Key.GetPubKey()));
        m_coinbase_txns.push_back(b.vtx[0]);
      }
    }

    std::unique_ptr<CWallet> wallet;
    CKey coinbase2Key;
};

CScript CreateStoreRecordScript(CScript& pubkeyhash, std::string id, std::string value)
{
  const valtype idVal = string2Valtype(id);
  const valtype valueVal = string2Valtype(value);
  return CRegisterScript::buildStoreRecord(pubkeyhash, idVal, valueVal);
}

int CountRegisterCoins(CWallet& wallet)
{
    int nRegisterCoins = 0;
    for (const auto& item : wallet.mapWallet) {
        const CWalletTx& tx = item.second;
        for (unsigned i = 0; i < tx.tx->vout.size (); ++i) {
            const CRegisterScript cur(tx.tx->vout[i].scriptPubKey);
            if (cur.isRegisterOp()) {
                nRegisterCoins++;
                break;
            }
        }
    }
    return nRegisterCoins;
}

} // anonymous namespace


BOOST_FIXTURE_TEST_CASE(RegisterOutputsAreNotForStaking, RegisterOutputsAreNotForStakingSetup)
{
    std::vector<COutput> availableCoins4Staking;

    // lets mature everything that is on our wallet
    GenerateEmptyBlock(COINBASE_MATURITY);

    wallet->AvailableCoinsForStaking(availableCoins4Staking);
    BOOST_CHECK_EQUAL(availableCoins4Staking.size(), 501);

    CScript pubkeyscript = GetScriptForDestination(coinbaseKey.GetPubKey().GetID());

    // tx w/ 1 input and 3 inputs (2 + 1 change)
    CWalletTx coinTx0 = AddTx({
        CRecipient{pubkeyscript, 100 * COIN, false}
        , CRecipient{pubkeyscript, 100 * COIN, false}
    });
    // tx w/ 1 input and 4 inputs (3 + 1 change)
    CWalletTx coinTx1 = AddTx({
        CRecipient{pubkeyscript, 100 * COIN, false}
        , CRecipient{pubkeyscript, 100 * COIN, false}
        , CRecipient{pubkeyscript, 100 * COIN, false}
    });

    // availableCoins4Staking expected: 500 (501-1)
    // coinTx0 consumes 1 coinbase since there is no other input available
    // coinTx1 consumes change output from coinTx0
    wallet->AvailableCoinsForStaking(availableCoins4Staking);
    BOOST_CHECK_EQUAL(availableCoins4Staking.size(), 500);

    GenerateEmptyBlock(COINBASE_MATURITY);

    // availableCoins4Staking expected: 506 (500 + 2 + 4)
    // coinTx0 created 3 ouputs (but 1 was consumed by coinTx1 already)
    // coinTx1 created 4 outputs
    wallet->AvailableCoinsForStaking(availableCoins4Staking);
    BOOST_CHECK_EQUAL(availableCoins4Staking.size(), 506);

    // tx w/ 1 input and 2 inputs (op_register and change)
    CWalletTx storeTx0 = AddTx({CRecipient{CreateStoreRecordScript(pubkeyscript, "some-id-0", "some-value"), 0, false}});

    BOOST_CHECK_EQUAL(CountRegisterCoins(*wallet), 1);

    // availableCoins4Staking expected: 505 (506-1)
    // will consume 1 of the existing (all mature) coins
    wallet->AvailableCoinsForStaking(availableCoins4Staking);
    BOOST_CHECK_EQUAL(availableCoins4Staking.size(), 505);

    // after 500 everything is matured
    GenerateEmptyBlock(COINBASE_MATURITY);

    // availableCoins4Staking expected: 506 (505+1)
    // storeTx0 created 1 store out (not stakable) and 1 output for change
    wallet->AvailableCoinsForStaking(availableCoins4Staking);
    BOOST_CHECK_EQUAL(availableCoins4Staking.size(), 506);

    // same id is discarded (wont count)
    CWalletTx storeTx1 = AddTx({
        CRecipient{CreateStoreRecordScript(pubkeyscript, "some-id-0", "some-value"), 0, false}
    });
    // +1 non stakable
    CWalletTx storeTx2 = AddTx({
        CRecipient{CreateStoreRecordScript(pubkeyscript, "some-id-1", "some-value"), 0, false}
    });

    BOOST_CHECK_EQUAL(CountRegisterCoins(*wallet), 3);

    // availableCoins4Staking expected: 505 (506-1)
    // storeTx1 consumes 1 mature out
    // WHAT HAPPENS
        // storeTx2 consumes storeTx1 out
    // WHAT SHOULD
        // storeTx2 is rejected because key is duplicated and will be rejected
    wallet->AvailableCoinsForStaking(availableCoins4Staking);
    BOOST_CHECK_EQUAL(availableCoins4Staking.size(), 505);

    // after 500 everything is matured
    GenerateEmptyBlock(COINBASE_MATURITY);

    // availableCoins4Staking expected: 506 (505+1)
    // storeTx1 and storeTx2 each one created an out for change
    // storeTx1 was already consumed
    wallet->AvailableCoinsForStaking(availableCoins4Staking);
    BOOST_CHECK_EQUAL(availableCoins4Staking.size(), 506);

    // 1 input and 4 outputs (w/ change)
    CWalletTx coinTx2 = AddTx({
        CRecipient{pubkeyscript, 1 * COIN, false}
        , CRecipient{pubkeyscript, 1 * COIN, false}
        , CRecipient{pubkeyscript, 1 * COIN, false}
    });

    // availableCoins4Staking expected: 505 (506-1)
    // coinTx0 consumes 1 mature output
    wallet->AvailableCoinsForStaking(availableCoins4Staking);
    BOOST_CHECK_EQUAL(availableCoins4Staking.size(), 505);

    // after 500 everything is matured
    GenerateEmptyBlock(COINBASE_MATURITY);

    wallet->AvailableCoinsForStaking(availableCoins4Staking);
    BOOST_CHECK_EQUAL(CountRegisterCoins(*wallet), 3);
    BOOST_CHECK_EQUAL(availableCoins4Staking.size(), 509);

    // register_script with amount is also not stakable
    // THIS ONE IS THE MOST IMPORTANT since the others are be ignored because nAmount = 0
    CWalletTx storeTx3 = AddTx({
        CRecipient{CreateStoreRecordScript(pubkeyscript, "some-id-2", "some-value"), 100, false}
    });

    // availableCoins4Staking expected: 508 (509-1)
    // the storeTx3 consumed a mature vout
    wallet->AvailableCoinsForStaking(availableCoins4Staking);
    BOOST_CHECK_EQUAL(CountRegisterCoins(*wallet), 4);
    BOOST_CHECK_EQUAL(availableCoins4Staking.size(), 508);

    // after 500 everything is matured
    GenerateEmptyBlock(COINBASE_MATURITY);

    // availableCoins4Staking expected: 509 (508+1)
    // the storeTx3 change output
    wallet->AvailableCoinsForStaking(availableCoins4Staking);
    BOOST_CHECK_EQUAL(CountRegisterCoins(*wallet), 4);
    BOOST_CHECK_EQUAL(availableCoins4Staking.size(), 509);
}

BOOST_AUTO_TEST_SUITE_END ()
