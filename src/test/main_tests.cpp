// Copyright (c) 2014-2018 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <chainparams.h>
#include <validation.h>
#include <net.h>

#include <test/test_bitcoin.h>

#include <boost/signals2/signal.hpp>
#include <boost/test/unit_test.hpp>

BOOST_FIXTURE_TEST_SUITE(main_tests, TestingSetup)

static void TestBlockSubsidyHalvings(const Consensus::Params& consensusParams)
{
    CAmount initialSubsidy = 22 * COIN;

    // before 1st halving
    int firstPOSBlock = consensusParams.nLastPOWBlock + 1;
    BOOST_CHECK_EQUAL(GetBlockSubsidy(firstPOSBlock, consensusParams), initialSubsidy);

    // from 1st halving till 20th "halving" is linear, i.e. -1
    CAmount nPreviousSubsidy = initialSubsidy;
    int nHalvings = 1;
    for (; nHalvings <= 20; nHalvings++) {
        int nHeight = firstPOSBlock + nHalvings * consensusParams.nSubsidyHalvingInterval;
        CAmount nSubsidy = GetBlockSubsidy(nHeight, consensusParams);
        BOOST_CHECK(nSubsidy <= initialSubsidy);
        BOOST_CHECK_EQUAL(nSubsidy, nPreviousSubsidy - COIN);
        nPreviousSubsidy = nSubsidy;
    }

    initialSubsidy = 2 * COIN;

    // from 21st till 47th "halving" is regular / 2
    for (; nHalvings <= 47; nHalvings++) {
        int nHeight = firstPOSBlock + nHalvings * consensusParams.nSubsidyHalvingInterval;
        CAmount nSubsidy = GetBlockSubsidy(nHeight, consensusParams);
        BOOST_CHECK(nSubsidy <= initialSubsidy);
        BOOST_CHECK_EQUAL(nSubsidy, nPreviousSubsidy / 2);
        nPreviousSubsidy = nSubsidy;
    }

    // from 48th forever is 0
    BOOST_CHECK_EQUAL(GetBlockSubsidy(48 * consensusParams.nSubsidyHalvingInterval + consensusParams.nLastPOWBlock + 1, consensusParams), 0);
}

static void TestBlockSubsidyHalvings(int nSubsidyHalvingInterval)
{
    Consensus::Params consensusParams;
    consensusParams.nSubsidyHalvingInterval = nSubsidyHalvingInterval;
    TestBlockSubsidyHalvings(consensusParams);
}

BOOST_AUTO_TEST_CASE(block_subsidy_test)
{
    const auto chainParams = CreateChainParams(CBaseChainParams::MAIN);
    TestBlockSubsidyHalvings(chainParams->GetConsensus()); // As in main
    TestBlockSubsidyHalvings(150); // As in regtest
    TestBlockSubsidyHalvings(1000); // Just another interval
}

BOOST_AUTO_TEST_CASE(subsidy_limit_test)
{
    const auto chainParams = CreateChainParams(CBaseChainParams::MAIN);
    const Consensus::Params& consensusParams = chainParams->GetConsensus();
    CAmount nSum = 0;
    for (int nHeight = 1; nHeight < 14000000; nHeight++) {
        CAmount nSubsidy = GetBlockSubsidy(nHeight, consensusParams);

        if (nHeight <= consensusParams.nLastPOWBlock){
            BOOST_CHECK_EQUAL(nSubsidy, 2457717420000); // 24577.1742
        }
        // no halving
        else if (nHeight - consensusParams.nLastPOWBlock <= consensusParams.nSubsidyHalvingInterval){
            BOOST_CHECK_EQUAL(nSubsidy, 22 * COIN);
        }
        // from 1st until 20th halving
        else if (nHeight - consensusParams.nLastPOWBlock <= consensusParams.nSubsidyHalvingInterval * 21){
            int halvings = (nHeight - consensusParams.nLastPOWBlock - 1) / consensusParams.nSubsidyHalvingInterval;
            BOOST_CHECK_EQUAL(nSubsidy, (22 - halvings) * COIN);
        }
        // from 21st until 47th halving
        else if (nHeight - consensusParams.nLastPOWBlock <= consensusParams.nSubsidyHalvingInterval * 48){
            int halvings = (nHeight - consensusParams.nLastPOWBlock - 1) / consensusParams.nSubsidyHalvingInterval;
            BOOST_CHECK_EQUAL(nSubsidy, (2 * COIN) >> (halvings - 20));
        }
        // from 48th halving
        else {
            BOOST_CHECK_EQUAL(nSubsidy, 0);
        }
        nSum += nSubsidy;
        BOOST_CHECK(MoneyRange(nSum));
    }
    BOOST_CHECK_EQUAL(nSum, CAmount{18546512097043500});
}

static bool ReturnFalse() { return false; }
static bool ReturnTrue() { return true; }

BOOST_AUTO_TEST_CASE(test_combiner_all)
{
    boost::signals2::signal<bool (), CombinerAll> Test;
    BOOST_CHECK(Test());
    Test.connect(&ReturnFalse);
    BOOST_CHECK(!Test());
    Test.connect(&ReturnTrue);
    BOOST_CHECK(!Test());
    Test.disconnect(&ReturnFalse);
    BOOST_CHECK(Test());
    Test.disconnect(&ReturnTrue);
    BOOST_CHECK(Test());
}
BOOST_AUTO_TEST_SUITE_END()
