// Copyright (c) 2011-2018 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <chainparams.h>
#include <coins.h>
#include <consensus/consensus.h>
#include <consensus/merkle.h>
#include <consensus/tx_verify.h>
#include <consensus/validation.h>
#include <validation.h>
#include <miner.h>
#include <policy/policy.h>
#include <pubkey.h>
#include <script/standard.h>
#include <txmempool.h>
#include <uint256.h>
#include <util.h>
#include <utilstrencodings.h>

#include <test/test_bitcoin.h>

#include <memory>

#include <boost/test/unit_test.hpp>

BOOST_FIXTURE_TEST_SUITE(miner_tests, TestingSetup)

// BOOST_CHECK_EXCEPTION predicates to check the specific validation error
class HasReason {
public:
    HasReason(const std::string& reason) : m_reason(reason) {}
    bool operator() (const std::runtime_error& e) const {
        return std::string(e.what()).find(m_reason) != std::string::npos;
    };
private:
    const std::string m_reason;
};

static CFeeRate blockMinFeeRate = CFeeRate(DEFAULT_BLOCK_MIN_TX_FEE);

static BlockAssembler AssemblerForTest(const CChainParams& params) {
    BlockAssembler::Options options;

    options.nBlockMaxWeight = dgpMaxBlockWeight;
    options.blockMinFeeRate = blockMinFeeRate;
    return BlockAssembler(params, options);
}

static
struct {
    unsigned char extranonce;
    unsigned int nonce;
} blockinfo[] = {
        {4, 0x0001f487}, {2, 0x0004d22c}, {1, 0x00081491}, {1, 0x000836cf},
        {2, 0x0008a45d}, {2, 0x000a5e62}, {1, 0x000aa6bc}, {2, 0x000ab21c},
        {2, 0x000c6f7e}, {1, 0x000c7346}, {1, 0x0010a716}, {2, 0x00117b48},
        {2, 0x00133288}, {1, 0x00146da5}, {2, 0x001514eb}, {2, 0x0015b6eb},
        {1, 0x0016a50f}, {2, 0x0018e4c0}, {1, 0x00194300}, {1, 0x0019ef14},
        {3, 0x001a2437}, {2, 0x001a8990}, {2, 0x001ad18b}, {1, 0x001bad50},
        {2, 0x001c1a21}, {1, 0x001cb303}, {2, 0x001ce8cf}, {2, 0x001fc16a},
        {2, 0x0023cacf}, {2, 0x00244f31}, {2, 0x0025809f}, {2, 0x00267ae8},
        {1, 0x002690d2}, {2, 0x0026a5cc}, {2, 0x0026b69c}, {1, 0x0026f2d0},
        {2, 0x002700b9}, {1, 0x002715f6}, {2, 0x00272448}, {1, 0x00292755},
        {1, 0x00296762}, {3, 0x00298785}, {2, 0x002a45da}, {5, 0x002a55ac},
        {1, 0x002a9b8a}, {5, 0x002af375}, {1, 0x002b1c31}, {1, 0x002b2901},
        {1, 0x002b4601}, {2, 0x002dca64}, {1, 0x002dfe30}, {1, 0x002e1201},
        {1, 0x002e97b1}, {1, 0x002e9b68}, {5, 0x002f088c}, {5, 0x00329ca1},
        {1, 0x003381f9}, {1, 0x0035aad5}, {6, 0x0038516d}, {2, 0x003855af},
        {2, 0x003a9c19}, {1, 0x003b035e}, {1, 0x003bca49}, {1, 0x003c79e3},
        {2, 0x003d980f}, {2, 0x003e1ea9}, {1, 0x003ea483}, {1, 0x00401c43},
        {1, 0x00402547}, {5, 0x004055c6}, {5, 0x00407375}, {1, 0x00408d4d},
        {1, 0x00428085}, {2, 0x0042f44a}, {2, 0x00461a73}, {1, 0x00487d79},
        {2, 0x004891e6}, {1, 0x0048f2bd}, {2, 0x004976fb}, {2, 0x004b03c3},
        {1, 0x004c38a4}, {1, 0x004c5f70}, {1, 0x004ca749}, {5, 0x004e21f2},
        {1, 0x004e9071}, {1, 0x004ef6c0}, {1, 0x00500d75}, {1, 0x00504ae7},
        {1, 0x005203e9}, {1, 0x005263fb}, {1, 0x00528007}, {2, 0x00557a25},
        {0, 0x0055bfaf}, {1, 0x0055d775}, {2, 0x0055e46f}, {2, 0x0057536e},
        {2, 0x00582fd8}, {1, 0x005afb9e}, {1, 0x005bfffc}, {1, 0x005cb1d3},
        {1, 0x0060035c}, {1, 0x00618519}, {1, 0x00619f83}, {5, 0x0061d5a6},
        {2, 0x006497fd}, {1, 0x0065e9c3}, {1, 0x00662771}, {1, 0x00662b81},
        {2, 0x0066636f}, {2, 0x0066e2d1}, {4, 0x00676db6}, {2, 0x0067d7d9},
        {1, 0x006b6bf0}, {1, 0x006de68a}, {2, 0x006ea884}, {2, 0x006ed34a},
        {1, 0x006f724a}, {2, 0x00743754}, {2, 0x0076927c}, {1, 0x007706ff},
        {1, 0x0078e005}, {2, 0x0079b225}, {2, 0x007a3a61}, {1, 0x007a3b32},
        {2, 0x007b79ec}, {2, 0x007d8c98}, {1, 0x007ec736}, {2, 0x007ed02f},
        {1, 0x008030da}, {1, 0x008288cd}, {3, 0x0083bddf}, {2, 0x00851a11},
        {2, 0x008589bf}, {1, 0x0085e18c}, {2, 0x008726c6}, {1, 0x0088127a},
        {2, 0x0088810f}, {2, 0x0089dba9}, {2, 0x008a2bc3}, {2, 0x008a3149},
        {2, 0x008a6791}, {2, 0x008ca2b5}, {1, 0x008e1ee2}, {2, 0x008f2ed2},
        {2, 0x00909f8f}, {1, 0x00926413}, {2, 0x00969f16}, {1, 0x00993b99},
        {2, 0x0099d5d5}, {1, 0x009a3c8b}, {1, 0x009c3a88}, {3, 0x009df3f5},
        {2, 0x00a12827}, {5, 0x00a1a3b0}, {1, 0x00a26cd9}, {5, 0x00a2702d},
        {1, 0x00a2c076}, {1, 0x00a2c082}, {1, 0x00a3eff8}, {2, 0x00a3f433},
        {1, 0x00a40a69}, {1, 0x00a4b8b4}, {1, 0x00a5788d}, {1, 0x00a6c80c},
        {5, 0x00a73c82}, {5, 0x00a7caa1}, {1, 0x00a9f7be}, {1, 0x00aaa8d8},
        {6, 0x00abf989}, {2, 0x00ae7af3}, {2, 0x00aee0c6}, {1, 0x00af1ceb},
        {1, 0x00b08e38}, {1, 0x00b19870}, {2, 0x00b3664a}, {2, 0x00b59765},
        {1, 0x00b6d08f}, {1, 0x00b73596}, {1, 0x00b82420}, {5, 0x00bf5cb6},
        {5, 0x00bf9fe5}, {1, 0x00c1b8f8}, {1, 0x00c259e7}, {2, 0x00c52baf},
        {2, 0x00c5f71b}, {1, 0x00c61190}, {2, 0x00c6e7ba}, {1, 0x00c732a0},
        {2, 0x00c80eb8}, {2, 0x00c85b4d}, {1, 0x00c869d6}, {1, 0x00ca2693},
        {1, 0x00cab2d0}, {5, 0x00cb9eb9}, {1, 0x00cbd122}, {1, 0x00ce15f8},
        {1, 0x00ce7d24}, {1, 0x00cf3ef2}, {1, 0x00cf721e}, {1, 0x00cf765b},
        {1, 0x00cfb512}, {2, 0x00d2bcd4}, {0, 0x00d3d682}, {1, 0x00d5c9b8},
        {2, 0x00d9aa81}, {2, 0x00da658b}, {2, 0x00db9cf4}, {1, 0x00dcaea8},
        {1, 0x00de51a4}, {1, 0x00deafd9}, {1, 0x00dfad16}, {1, 0x00e033ef},
        {1, 0x00e130f7}, {5, 0x00e1c178}, {2, 0x00e217c3}, {1, 0x00e316dc},
        {1, 0x00e3fd5b}, {1, 0x00e53ac6}, {2, 0x00e5fe45}, {2, 0x00e696d1},
        {4, 0x00e7ee6d}, {2, 0x00e83595}, {1, 0x00e8f4cf}, {1, 0x00ea9245},
        {2, 0x00eb4814}, {2, 0x00eb7447}, {1, 0x00ebb076}, {2, 0x00ebe2ef},
        {2, 0x00eec62a}, {1, 0x00ef8110}, {1, 0x00efca81}, {2, 0x00f15ae1},
        {2, 0x00f17475}, {1, 0x00f25ac6}, {2, 0x00f518fc}, {2, 0x00f57501},
        {1, 0x00f5baa2}, {2, 0x00f769eb}, {1, 0x00f93acc}, {1, 0x00f93ee1},
        {3, 0x00f9a958}, {2, 0x00fd0275}, {2, 0x00fdc2d6}, {1, 0x00fe4cdc},
        {2, 0x00feaca1}, {1, 0x01009a6b}, {2, 0x01019486}, {2, 0x0101d8e0},
        {2, 0x0103543d}, {2, 0x0104c888}, {2, 0x0106d1a5}, {2, 0x0107daf2},
        {1, 0x01083428}, {2, 0x010a6b97}, {2, 0x010cfb38}, {1, 0x010de48f},
        {2, 0x010e7380}, {1, 0x0112846b}, {2, 0x01135eea}, {1, 0x01184423},
        {1, 0x0118ec20}, {3, 0x011afea8}, {2, 0x011d2f31}, {5, 0x011d344e},
        {1, 0x011da86b}, {5, 0x011daf7f}, {1, 0x011f5180}, {1, 0x01208735},
        {1, 0x012114c1}, {2, 0x01223756}, {1, 0x01251182}, {1, 0x01254430},
        {1, 0x012942de}, {1, 0x012aed35}, {5, 0x012afa89}, {5, 0x012bbe93},
        {1, 0x012be580}, {1, 0x012e3268}, {6, 0x012f18fb}, {2, 0x012fd35b},
        {2, 0x01315af7}, {1, 0x01315d39}, {1, 0x0132c94f}, {1, 0x013333cb},
        {2, 0x0133ad42}, {2, 0x013462c4}, {1, 0x0135567f}, {1, 0x01355c69},
        {1, 0x0135bdaf}, {5, 0x01376753}, {5, 0x0137a92f}, {1, 0x013a1ee0},
        {1, 0x013ac556}, {2, 0x013b47a0}, {2, 0x013bac0a}, {1, 0x013cee72},
        {2, 0x013cff98}, {1, 0x013d7255}, {2, 0x013f1000}, {2, 0x013f10c6},
        {1, 0x013fcd0b}, {1, 0x01404ca7}, {1, 0x014120e3}, {5, 0x0141a663},
        {1, 0x0141d2b8}, {1, 0x014383dd}, {1, 0x01452e48}, {1, 0x0145ccbf},
        {1, 0x01469e74}, {1, 0x01485048}, {1, 0x0148ccab}, {2, 0x01495697},
        {0, 0x014b2483}, {1, 0x014b7aa8}, {2, 0x014e1676}, {2, 0x014eba95},
        {2, 0x014f2146}, {1, 0x0150680a}, {1, 0x0150b343}, {1, 0x0150eb93},
        {1, 0x01510a8c}, {1, 0x0151b0db}, {1, 0x01531306}, {5, 0x015688d6},
        {2, 0x015787d6}, {1, 0x015a7ab2}, {1, 0x015b1c60}, {1, 0x015d7928},
        {2, 0x01613a04}, {2, 0x0166de6a}, {4, 0x0167860a}, {2, 0x01683e17},
        {1, 0x0168f88a}, {1, 0x016982a4}, {2, 0x0169fbda}, {2, 0x016d1a6c},
        {1, 0x016f58f5}, {2, 0x016fe04c}, {2, 0x017237e3}, {1, 0x01725d65},
        {1, 0x0173eff2}, {2, 0x0175a731}, {2, 0x01762560}, {1, 0x01774bd2},
        {2, 0x01784b4a}, {2, 0x0178a92b}, {1, 0x0179261a}, {2, 0x017a2c85},
        {1, 0x017a40b3}, {1, 0x017a44f1}, {3, 0x017b2990}, {2, 0x017ec33b},
        {2, 0x01804056}, {1, 0x018274f9}, {2, 0x018345bd}, {1, 0x0183cece},
        {2, 0x0183e1a9}, {2, 0x0184f4fd}, {2, 0x01876666}, {2, 0x01887102},
        {2, 0x0189c413}, {2, 0x018a4095}, {1, 0x018a699e}, {2, 0x018a83aa},
        {2, 0x018cba63}, {1, 0x018d9e3f}, {2, 0x018e7691}, {1, 0x01904b1f},
        {2, 0x0193139d}, {1, 0x0193b81a}, {1, 0x01968dca}, {3, 0x01972cce},
        {2, 0x0197717f}, {5, 0x0198ac71}, {1, 0x0198fa4b}, {5, 0x0199554f},
        {1, 0x019d444c}, {1, 0x019de2e2}, {1, 0x019ea813}, {2, 0x01a044d3},
        {1, 0x01a07a73}, {1, 0x01a0d844}, {1, 0x01ad497c}, {1, 0x01af0bf0},
        {5, 0x01affcbf}, {5, 0x01b098b1}, {1, 0x01b1c8eb}, {1, 0x01b1ed9f},
        {6, 0x01b4cd15}, {2, 0x01b6ee79}, {2, 0x01b74b81}, {1, 0x01b75c11},
        {1, 0x01b916ac}, {1, 0x01b94fb0}, {2, 0x01b959a7}, {2, 0x01ba754b},
        {1, 0x01bab9b5}, {1, 0x01bb3314}, {1, 0x01bb3c21}, {5, 0x01bc1bb6},
        {5, 0x01bc1c84}, {1, 0x01bd5aa7}, {1, 0x01be4171}, {2, 0x01be6c5f},
        {2, 0x01bf9e19}, {1, 0x01c07c92}, {2, 0x01c2deb9}, {1, 0x01c391c1},
        {2, 0x01c605e9}, {2, 0x01c74ea9}, {1, 0x01c7fe23}, {1, 0x01ca1c9a},
        {1, 0x01ca6543}, {5, 0x01cae772}, {1, 0x01cc9692}, {1, 0x01ccd966},
        {1, 0x01cd5b11}, {1, 0x01ceedc7}, {1, 0x01cf2db3}, {1, 0x01cfef73},
        {1, 0x01d23bc0}, {2, 0x01d2768a}, {0, 0x01d43205}, {1, 0x01d62f3e},
        {2, 0x01d6aeb3}, {2, 0x01d87c2b}, {2, 0x01da4afd}, {1, 0x01da7835},
        {1, 0x01dae34f}, {1, 0x01dc9048}, {1, 0x01dd42a0}, {1, 0x01df2cc2},
        {1, 0x01df5aa0}, {5, 0x01e01e85}, {2, 0x01e029af}, {1, 0x01e04ea6},
        {1, 0x01e27e1f}, {1, 0x01e2a0ba}, {2, 0x01e38777}, {2, 0x01e3a65a},
        {4, 0x01e408f1}, {2, 0x01e581cd}, {1, 0x01e5ed16}, {1, 0x01e637f7},
        {2, 0x01e63a63}, {2, 0x01e77734}, {1, 0x01e80ea3}, {2, 0x01e83a9d},
        {2, 0x01ecba9d}, {1, 0x01ecf757}, {1, 0x01ed48db}, {2, 0x01efa159},
        {2, 0x01f2b56b}, {1, 0x01f61ca1}, {2, 0x01f68dc7}, {2, 0x01f693e3},
        {1, 0x01f6aaca}, {2, 0x01f70560}, {1, 0x01f74ada}, {1, 0x01f8816b},
        {3, 0x01f8f04f}, {2, 0x01f97b9a}, {2, 0x01f9faf0}, {1, 0x01fb3f4b},
        {2, 0x01fc68e2}, {1, 0x01fc9ded}, {2, 0x01fd2ab9}, {2, 0x01fe0e6b},
        {2, 0x020095d5}, {2, 0x0200b8b4}, {2, 0x0204e5ff}, {2, 0x020501c9},
        {1, 0x02063be1}, {2, 0x02066ad9}, {2, 0x020764e1}, {1, 0x020a2f8e},
        {2, 0x020c7157}, {1, 0x020cc68b}, {2, 0x020e2cf7}, {1, 0x0211cfe5},
        {1, 0x0211f862}, {3, 0x02126516}, {2, 0x0215575c}, {5, 0x0215b969},
        {1, 0x02171442}, {5, 0x0217c088}, {1, 0x021823bc}, {1, 0x021863db},
        {1, 0x0218822d}, {2, 0x0219ea1a}, {1, 0x021a2b4a}, {1, 0x021da882},
        {1, 0x021e6586}, {1, 0x021f93ba}, {5, 0x021fd5cc}, {5, 0x0220190a},
        {1, 0x02202840}, {1, 0x0220bd12}, {6, 0x02224165}, {2, 0x02239674},
        {2, 0x02239fcc}, {1, 0x022528c1}, {1, 0x022774da}, {1, 0x0227d283},
        {2, 0x022845bd}, {2, 0x022b2447}, {1, 0x022b9ec8}, {1, 0x022f88f5},
        {1, 0x02309a1c}, {5, 0x02319af3}, {5, 0x0231c1a0}, {1, 0x0234f1a4},
        {1, 0x0235d7e1}, {2, 0x023642f3}, {2, 0x023832da}, {1, 0x0238cb03},
        {2, 0x0239a009}, {1, 0x023e16b3}, {2, 0x024180e8}, {2, 0x02427d61},
        {1, 0x0243867d}, {1, 0x0245dc26}, {1, 0x02464454}, {5, 0x024662d7},
        {1, 0x02490594}, {1, 0x02497838}, {1, 0x024a0fbd}, {1, 0x024ac41d},
        {1, 0x024b6c11}, {1, 0x024bc687}, {1, 0x024cb63d}, {2, 0x024e369e},
        {0, 0x024e5a9e}, {1, 0x02521a62}, {2, 0x025287b9}, {2, 0x0254b2f3},
        {2, 0x02557661}, {1, 0x02568267}, {1, 0x0257c90f}, {1, 0x025a87b6},
        {1, 0x025aeb38}, {1, 0x025ca3cf}, {1, 0x025fc16b}, {5, 0x02601332},
        {2, 0x0260cc08}, {1, 0x0261449c}, {1, 0x0261abf2}, {1, 0x0261af1b},
        {2, 0x0261b47a}, {2, 0x0263e297},
};

static CBlockIndex CreateBlockIndex(int nHeight)
{
    CBlockIndex index;
    index.nHeight = nHeight;
    index.pprev = chainActive.Tip();
    return index;
}

static bool TestSequenceLocks(const CTransaction &tx, int flags)
{
    LOCK(mempool.cs);
    return CheckSequenceLocks(tx, flags);
}

// Test suite for ancestor feerate transaction selection.
// Implemented as an additional function, rather than a separate test case,
// to allow reusing the blockchain created in CreateNewBlock_validity.
static void TestPackageSelection(const CChainParams& chainparams, const CScript& scriptPubKey, const std::vector<CTransactionRef>& txFirst) EXCLUSIVE_LOCKS_REQUIRED(::mempool.cs)
{
    // Test the ancestor feerate transaction selection.
    TestMemPoolEntryHelper entry;

    // Test that a medium fee transaction will be selected after a higher fee
    // rate package with a low fee rate parent.
    CMutableTransaction tx;
    tx.vin.resize(1);
    tx.vin[0].scriptSig = CScript() << OP_1;
    tx.vin[0].prevout.hash = txFirst[0]->GetHash();
    tx.vin[0].prevout.n = 0;
    tx.vout.resize(1);
    tx.vout[0].nValue = 5000000000LL - 400000;
    // This tx has a low fee: 1000 satoshis
    uint256 hashParentTx = tx.GetHash(); // save this txid for later use
    mempool.addUnchecked(hashParentTx, entry.Fee(400000).Time(GetTime()).SpendsCoinbase(true).FromTx(tx));

    // This tx has a medium fee: 10000 satoshis
    tx.vin[0].prevout.hash = txFirst[1]->GetHash();
    tx.vout[0].nValue = 5000000000LL - 4000000;
    uint256 hashMediumFeeTx = tx.GetHash();
    mempool.addUnchecked(hashMediumFeeTx, entry.Fee(4000000).Time(GetTime()).SpendsCoinbase(true).FromTx(tx));

    // This tx has a high fee, but depends on the first transaction
    tx.vin[0].prevout.hash = hashParentTx;
    tx.vout[0].nValue = 5000000000LL - 10000 - 400000 * 50; // 50k satoshi fee
    uint256 hashHighFeeTx = tx.GetHash();
    mempool.addUnchecked(hashHighFeeTx, entry.Fee(400000 * 50).Time(GetTime()).SpendsCoinbase(false).FromTx(tx));

    std::unique_ptr<CBlockTemplate> pblocktemplate = AssemblerForTest(chainparams).CreateNewBlock(scriptPubKey);
    BOOST_CHECK(pblocktemplate->block.vtx[1]->GetHash() == hashParentTx);
    BOOST_CHECK(pblocktemplate->block.vtx[2]->GetHash() == hashHighFeeTx);
    BOOST_CHECK(pblocktemplate->block.vtx[3]->GetHash() == hashMediumFeeTx);

    // Test that a package below the block min tx fee doesn't get included
    tx.vin[0].prevout.hash = hashHighFeeTx;
    tx.vout[0].nValue = 5000000000LL - 10000 - 20000000; // 0 fee
    uint256 hashFreeTx = tx.GetHash();
    mempool.addUnchecked(hashFreeTx, entry.Fee(0).FromTx(tx));
    size_t freeTxSize = ::GetSerializeSize(tx, SER_NETWORK, PROTOCOL_VERSION);

    // Calculate a fee on child transaction that will put the package just
    // below the block min tx fee (assuming 1 child tx of the same size).
    CAmount feeToUse = blockMinFeeRate.GetFee(2*freeTxSize) - 1;

    tx.vin[0].prevout.hash = hashFreeTx;
    tx.vout[0].nValue = 5000000000LL - 10000 - 20000000 - feeToUse;
    uint256 hashLowFeeTx = tx.GetHash();
    mempool.addUnchecked(hashLowFeeTx, entry.Fee(feeToUse).FromTx(tx));
    pblocktemplate = AssemblerForTest(chainparams).CreateNewBlock(scriptPubKey);
    // Verify that the free tx and the low fee tx didn't get selected
    for (size_t i=0; i<pblocktemplate->block.vtx.size(); ++i) {
        BOOST_CHECK(pblocktemplate->block.vtx[i]->GetHash() != hashFreeTx);
        BOOST_CHECK(pblocktemplate->block.vtx[i]->GetHash() != hashLowFeeTx);
    }

    // Test that packages above the min relay fee do get included, even if one
    // of the transactions is below the min relay fee
    // Remove the low fee transaction and replace with a higher fee transaction
    mempool.removeRecursive(tx);
    tx.vout[0].nValue -= 2; // Now we should be just over the min relay fee
    hashLowFeeTx = tx.GetHash();
    mempool.addUnchecked(hashLowFeeTx, entry.Fee(feeToUse+2).FromTx(tx));
    pblocktemplate = AssemblerForTest(chainparams).CreateNewBlock(scriptPubKey);
    BOOST_CHECK(pblocktemplate->block.vtx[4]->GetHash() == hashFreeTx);
    BOOST_CHECK(pblocktemplate->block.vtx[5]->GetHash() == hashLowFeeTx);

    // Test that transaction selection properly updates ancestor fee
    // calculations as ancestor transactions get included in a block.
    // Add a 0-fee transaction that has 2 outputs.
    tx.vin[0].prevout.hash = txFirst[2]->GetHash();
    tx.vout.resize(2);
    tx.vout[0].nValue = 5000000000LL - 100000000;
    tx.vout[1].nValue = 100000000; // 1BTC output
    uint256 hashFreeTx2 = tx.GetHash();
    mempool.addUnchecked(hashFreeTx2, entry.Fee(0).SpendsCoinbase(true).FromTx(tx));

    // This tx can't be mined by itself
    tx.vin[0].prevout.hash = hashFreeTx2;
    tx.vout.resize(1);
    feeToUse = blockMinFeeRate.GetFee(freeTxSize);
    tx.vout[0].nValue = 5000000000LL - 100000000 - feeToUse;
    uint256 hashLowFeeTx2 = tx.GetHash();
    mempool.addUnchecked(hashLowFeeTx2, entry.Fee(feeToUse).SpendsCoinbase(false).FromTx(tx));
    pblocktemplate = AssemblerForTest(chainparams).CreateNewBlock(scriptPubKey);

    // Verify that this tx isn't selected.
    for (size_t i=0; i<pblocktemplate->block.vtx.size(); ++i) {
        BOOST_CHECK(pblocktemplate->block.vtx[i]->GetHash() != hashFreeTx2);
        BOOST_CHECK(pblocktemplate->block.vtx[i]->GetHash() != hashLowFeeTx2);
    }

    // This tx will be mineable, and should cause hashLowFeeTx2 to be selected
    // as well.
    tx.vin[0].prevout.n = 1;
    tx.vout[0].nValue = 100000000 - 4000000; // 10k satoshi fee
    mempool.addUnchecked(tx.GetHash(), entry.Fee(4000000).FromTx(tx));
    pblocktemplate = AssemblerForTest(chainparams).CreateNewBlock(scriptPubKey);
    BOOST_CHECK(pblocktemplate->block.vtx[8]->GetHash() == hashLowFeeTx2);
}
CAmount calculateReward(const CBlock& block){
    CAmount sumVout = 0, fee = 0;
    for(const CTransactionRef t : block.vtx){
        fee += pcoinsTip->GetValueIn(*t);
        sumVout += t->GetValueOut();
    }
    return sumVout - fee;
}

// NOTE: These tests rely on CreateNewBlock doing its own self-validation!
BOOST_AUTO_TEST_CASE(CreateNewBlock_validity)
{
    // Note that by default, these tests run with size accounting enabled.
    const auto chainParams = CreateChainParams(CBaseChainParams::MAIN);
    const CChainParams& chainparams = *chainParams;
    CScript scriptPubKey = CScript() << ParseHex("0460a8434e063949f5982699f62ff023b0ebe28af943d710cb7e34a6295992af8734f4e475e4bc135485d47357acfb89104d4ace25e9332e419b43af9db4691f0c") << OP_CHECKSIG;
    std::unique_ptr<CBlockTemplate> pblocktemplate;
    CMutableTransaction tx;
    CScript script;
    uint256 hash;
    TestMemPoolEntryHelper entry;
    entry.nFee = 11;
    entry.nHeight = 11;

    fCheckpointsEnabled = false;

    // Simple block creation, nothing special yet:
    BOOST_CHECK(pblocktemplate = AssemblerForTest(chainparams).CreateNewBlock(scriptPubKey));

    // We can't make transactions until we have inputs
    // Therefore, load 100 blocks :)
    int baseheight = 0;
    std::vector<CTransactionRef> txFirst;
    for (unsigned int i = 0; i < sizeof(blockinfo)/sizeof(*blockinfo); ++i)
    {
        CBlock *pblock = &pblocktemplate->block; // pointer for convenience
        {
            LOCK(cs_main);
            pblock->nVersion = 4; //use version 4 as we enable BIP34, BIP65 and BIP66 since genesis
            pblock->nTime = chainActive.Tip()->GetMedianTimePast()+1+i;
            CMutableTransaction txCoinbase(*pblock->vtx[0]);
            txCoinbase.nVersion = 1;
            txCoinbase.vin[0].scriptSig = CScript();
            txCoinbase.vin[0].scriptSig = CScript() << chainActive.Height()+1 << blockinfo[i].extranonce;
            txCoinbase.vout.resize(1); // Ignore the (optional) segwit commitment added by CreateNewBlock (as the hardcoded nonces don't account for this)
            txCoinbase.vout[0].scriptPubKey = CScript();
            pblock->vtx[0] = MakeTransactionRef(std::move(txCoinbase));
            if (txFirst.size() == 0)
                baseheight = chainActive.Height();
            if (txFirst.size() < 4)
                txFirst.push_back(pblock->vtx[0]);
            pblock->hashMerkleRoot = BlockMerkleRoot(*pblock);
            pblock->nNonce = blockinfo[i].nonce;
        }
        std::shared_ptr<const CBlock> shared_pblock = std::make_shared<const CBlock>(*pblock);
        BOOST_CHECK(ProcessNewBlock(chainparams, shared_pblock, true, nullptr));
        pblock->hashPrevBlock = pblock->GetHash();
    }

    LOCK(cs_main);
    LOCK(::mempool.cs);

    // Just to make sure we can still make simple blocks
    BOOST_CHECK(pblocktemplate = AssemblerForTest(chainparams).CreateNewBlock(scriptPubKey));

    const CAmount BLOCKSUBSIDY = 50*COIN;
    const CAmount LOWFEE = CENT;
    const CAmount HIGHFEE = COIN;
    const CAmount HIGHERFEE = 4*COIN;

    // block sigops > limit: 1000 CHECKMULTISIG + 1
    tx.vin.resize(1);
    // NOTE: OP_NOP is used to force 20 SigOps for the CHECKMULTISIG
    tx.vin[0].scriptSig = CScript() << OP_0 << OP_0 << OP_0 << OP_NOP << OP_CHECKMULTISIG << OP_1;
    tx.vin[0].prevout.hash = txFirst[0]->GetHash();
    tx.vin[0].prevout.n = 0;
    tx.vout.resize(1);
    tx.vout[0].nValue = BLOCKSUBSIDY;
    for (unsigned int i = 0; i < 1001; ++i)
    {
        tx.vout[0].nValue -= LOWFEE;
        hash = tx.GetHash();
        bool spendsCoinbase = i == 0; // only first tx spends coinbase
        // If we don't set the # of sig ops in the CTxMemPoolEntry, template creation fails
        mempool.addUnchecked(hash, entry.Fee(LOWFEE).Time(GetTime()).SpendsCoinbase(spendsCoinbase).FromTx(tx));
        tx.vin[0].prevout.hash = hash;
    }

    BOOST_CHECK_EXCEPTION(AssemblerForTest(chainparams).CreateNewBlock(scriptPubKey), std::runtime_error, HasReason("bad-blk-sigops"));
    mempool.clear();

    tx.vin[0].prevout.hash = txFirst[0]->GetHash();
    tx.vout[0].nValue = BLOCKSUBSIDY;
    for (unsigned int i = 0; i < 1001; ++i)
    {
        tx.vout[0].nValue -= LOWFEE;
        hash = tx.GetHash();
        bool spendsCoinbase = i == 0; // only first tx spends coinbase
        // If we do set the # of sig ops in the CTxMemPoolEntry, template creation passes
        mempool.addUnchecked(hash, entry.Fee(LOWFEE).Time(GetTime()).SpendsCoinbase(spendsCoinbase).SigOpsCost(80).FromTx(tx));
        tx.vin[0].prevout.hash = hash;
    }
    BOOST_CHECK(pblocktemplate = AssemblerForTest(chainparams).CreateNewBlock(scriptPubKey));
    mempool.clear();

    // block size > limit
    tx.vin[0].scriptSig = CScript();
    // 18 * (520char + DROP) + OP_1 = 9433 bytes
    std::vector<unsigned char> vchData(52);
    for (unsigned int i = 0; i < 18; ++i)
        tx.vin[0].scriptSig << vchData << OP_DROP;
    tx.vin[0].scriptSig << OP_1;
    tx.vin[0].prevout.hash = txFirst[0]->GetHash();
    tx.vout[0].nValue = BLOCKSUBSIDY;
    for (unsigned int i = 0; i < 128; ++i)
    {
        tx.vout[0].nValue -= LOWFEE;
        hash = tx.GetHash();
        bool spendsCoinbase = i == 0; // only first tx spends coinbase
        mempool.addUnchecked(hash, entry.Fee(LOWFEE).Time(GetTime()).SpendsCoinbase(spendsCoinbase).FromTx(tx));
        tx.vin[0].prevout.hash = hash;
    }
    BOOST_CHECK(pblocktemplate = AssemblerForTest(chainparams).CreateNewBlock(scriptPubKey));
    mempool.clear();

    // orphan in mempool, template creation fails
    hash = tx.GetHash();
    mempool.addUnchecked(hash, entry.Fee(LOWFEE).Time(GetTime()).FromTx(tx));
    BOOST_CHECK_EXCEPTION(AssemblerForTest(chainparams).CreateNewBlock(scriptPubKey), std::runtime_error, HasReason("bad-txns-inputs-missingorspent"));
    mempool.clear();

    // child with higher feerate than parent
    tx.vin[0].scriptSig = CScript() << OP_1;
    tx.vin[0].prevout.hash = txFirst[1]->GetHash();
    tx.vout[0].nValue = BLOCKSUBSIDY-HIGHFEE;
    hash = tx.GetHash();
    mempool.addUnchecked(hash, entry.Fee(HIGHFEE).Time(GetTime()).SpendsCoinbase(true).FromTx(tx));
    tx.vin[0].prevout.hash = hash;
    tx.vin.resize(2);
    tx.vin[1].scriptSig = CScript() << OP_1;
    tx.vin[1].prevout.hash = txFirst[0]->GetHash();
    tx.vin[1].prevout.n = 0;
    tx.vout[0].nValue = tx.vout[0].nValue+BLOCKSUBSIDY-HIGHERFEE; //First txn output + fresh coinbase - new txn fee
    hash = tx.GetHash();
    mempool.addUnchecked(hash, entry.Fee(HIGHERFEE).Time(GetTime()).SpendsCoinbase(true).FromTx(tx));
    BOOST_CHECK(pblocktemplate = AssemblerForTest(chainparams).CreateNewBlock(scriptPubKey));
    mempool.clear();

    // coinbase in mempool, template creation fails
    tx.vin.resize(1);
    tx.vin[0].prevout.SetNull();
    tx.vin[0].scriptSig = CScript() << OP_0 << OP_1;
    tx.vout[0].nValue = 0;
    hash = tx.GetHash();
    // give it a fee so it'll get mined
    mempool.addUnchecked(hash, entry.Fee(LOWFEE).Time(GetTime()).SpendsCoinbase(false).FromTx(tx));
    // Should throw bad-cb-multiple
    BOOST_CHECK_EXCEPTION(AssemblerForTest(chainparams).CreateNewBlock(scriptPubKey), std::runtime_error, HasReason("bad-cb-multiple"));
    mempool.clear();

    // double spend txn pair in mempool, template creation fails
    tx.vin[0].prevout.hash = txFirst[0]->GetHash();
    tx.vin[0].scriptSig = CScript() << OP_1;
    tx.vout[0].nValue = BLOCKSUBSIDY-HIGHFEE;
    tx.vout[0].scriptPubKey = CScript() << OP_1;
    hash = tx.GetHash();
    mempool.addUnchecked(hash, entry.Fee(HIGHFEE).Time(GetTime()).SpendsCoinbase(true).FromTx(tx));
    tx.vout[0].scriptPubKey = CScript() << OP_2;
    hash = tx.GetHash();
    mempool.addUnchecked(hash, entry.Fee(HIGHFEE).Time(GetTime()).SpendsCoinbase(true).FromTx(tx));
    BOOST_CHECK_EXCEPTION(AssemblerForTest(chainparams).CreateNewBlock(scriptPubKey), std::runtime_error, HasReason("bad-txns-inputs-missingorspent"));
    mempool.clear();

    // subsidy changing
    int nHeight = chainActive.Height();
    // Create an actual 251374-long block chain (without valid blocks).
    while (chainActive.Tip()->nHeight < 251374) {
        CBlockIndex* prev = chainActive.Tip();
        CBlockIndex* next = new CBlockIndex();
        next->phashBlock = new uint256(InsecureRand256());
        pcoinsTip->SetBestBlock(next->GetBlockHash());
        next->pprev = prev;
        next->nHeight = prev->nHeight + 1;
        next->BuildSkip();
        chainActive.SetTip(next);
    }
    BOOST_CHECK(pblocktemplate = AssemblerForTest(chainparams).CreateNewBlock(scriptPubKey, true, true));
    BOOST_CHECK(calculateReward(pblocktemplate->block) == 2200000000); // before halving

    // Extend to a 251375-long block chain.
    while (chainActive.Tip()->nHeight < 251375) {
        CBlockIndex* prev = chainActive.Tip();
        CBlockIndex* next = new CBlockIndex();
        next->phashBlock = new uint256(InsecureRand256());
        pcoinsTip->SetBestBlock(next->GetBlockHash());
        next->pprev = prev;
        next->nHeight = prev->nHeight + 1;
        next->BuildSkip();
        chainActive.SetTip(next);
    }

    BOOST_CHECK(pblocktemplate = AssemblerForTest(chainparams).CreateNewBlock(scriptPubKey, true, true));
    BOOST_CHECK(calculateReward(pblocktemplate->block) == 2100000000); // after 1 halving

    // invalid p2sh txn in mempool, template creation fails
    tx.vin[0].prevout.hash = txFirst[0]->GetHash();
    tx.vin[0].prevout.n = 0;
    tx.vin[0].scriptSig = CScript() << OP_1;
    tx.vout[0].nValue = BLOCKSUBSIDY-LOWFEE;
    script = CScript() << OP_0;
    tx.vout[0].scriptPubKey = GetScriptForDestination(CScriptID(script));
    hash = tx.GetHash();
    mempool.addUnchecked(hash, entry.Fee(LOWFEE).Time(GetTime()).SpendsCoinbase(true).FromTx(tx));
    tx.vin[0].prevout.hash = hash;
    tx.vin[0].scriptSig = CScript() << std::vector<unsigned char>(script.begin(), script.end());
    tx.vout[0].nValue -= LOWFEE;
    hash = tx.GetHash();
    mempool.addUnchecked(hash, entry.Fee(LOWFEE).Time(GetTime()).SpendsCoinbase(false).FromTx(tx));
    // Should throw block-validation-failed
    BOOST_CHECK_EXCEPTION(AssemblerForTest(chainparams).CreateNewBlock(scriptPubKey), std::runtime_error, HasReason("TestBlockValidity failed"));
    mempool.clear();

    // Delete the dummy blocks again.
    while (chainActive.Tip()->nHeight > nHeight) {
        CBlockIndex* del = chainActive.Tip();
        chainActive.SetTip(del->pprev);
        pcoinsTip->SetBestBlock(del->pprev->GetBlockHash());
        delete del->phashBlock;
        delete del;
    }

    // non-final txs in mempool
    SetMockTime(chainActive.Tip()->GetMedianTimePast()+1);
    int flags = LOCKTIME_VERIFY_SEQUENCE|LOCKTIME_MEDIAN_TIME_PAST;
    // height map
    std::vector<int> prevheights;

    // relative height locked
    tx.nVersion = 2;
    tx.vin.resize(1);
    prevheights.resize(1);
    tx.vin[0].prevout.hash = txFirst[0]->GetHash(); // only 1 transaction
    tx.vin[0].prevout.n = 0;
    tx.vin[0].scriptSig = CScript() << OP_1;
    tx.vin[0].nSequence = chainActive.Tip()->nHeight + 1; // txFirst[0] is the 2nd block
    prevheights[0] = baseheight + 1;
    tx.vout.resize(1);
    tx.vout[0].nValue = BLOCKSUBSIDY-HIGHFEE;
    tx.vout[0].scriptPubKey = CScript() << OP_1;
    tx.nLockTime = 0;
    hash = tx.GetHash();
    mempool.addUnchecked(hash, entry.Fee(HIGHFEE).Time(GetTime()).SpendsCoinbase(true).FromTx(tx));
    BOOST_CHECK(CheckFinalTx(tx, flags)); // Locktime passes
    BOOST_CHECK(!TestSequenceLocks(tx, flags)); // Sequence locks fail
    BOOST_CHECK(SequenceLocks(tx, flags, &prevheights, CreateBlockIndex(chainActive.Tip()->nHeight + 2))); // Sequence locks pass on 2nd block

    // relative time locked
    tx.vin[0].prevout.hash = txFirst[1]->GetHash();
    tx.vin[0].nSequence = CTxIn::SEQUENCE_LOCKTIME_TYPE_FLAG | (((chainActive.Tip()->GetMedianTimePast()+1-chainActive[1]->GetMedianTimePast()) >> CTxIn::SEQUENCE_LOCKTIME_GRANULARITY) + 1); // txFirst[1] is the 3rd block
    prevheights[0] = baseheight + 2;
    hash = tx.GetHash();
    mempool.addUnchecked(hash, entry.Time(GetTime()).FromTx(tx));
    BOOST_CHECK(CheckFinalTx(tx, flags)); // Locktime passes
    BOOST_CHECK(!TestSequenceLocks(tx, flags)); // Sequence locks fail

    for (int i = 0; i < CBlockIndex::nMedianTimeSpan; i++)
        chainActive.Tip()->GetAncestor(chainActive.Tip()->nHeight - i)->nTime += 512; //Trick the MedianTimePast
    BOOST_CHECK(SequenceLocks(tx, flags, &prevheights, CreateBlockIndex(chainActive.Tip()->nHeight + 1))); // Sequence locks pass 512 seconds later
    for (int i = 0; i < CBlockIndex::nMedianTimeSpan; i++)
        chainActive.Tip()->GetAncestor(chainActive.Tip()->nHeight - i)->nTime -= 512; //undo tricked MTP

    // absolute height locked
    tx.vin[0].prevout.hash = txFirst[2]->GetHash();
    tx.vin[0].nSequence = CTxIn::SEQUENCE_FINAL - 1;
    prevheights[0] = baseheight + 3;
    tx.nLockTime = chainActive.Tip()->nHeight + 1;
    hash = tx.GetHash();
    mempool.addUnchecked(hash, entry.Time(GetTime()).FromTx(tx));
    BOOST_CHECK(!CheckFinalTx(tx, flags)); // Locktime fails
    BOOST_CHECK(TestSequenceLocks(tx, flags)); // Sequence locks pass
    BOOST_CHECK(IsFinalTx(tx, chainActive.Tip()->nHeight + 2, chainActive.Tip()->GetMedianTimePast())); // Locktime passes on 2nd block

    // absolute time locked
    tx.vin[0].prevout.hash = txFirst[3]->GetHash();
    tx.nLockTime = chainActive.Tip()->GetMedianTimePast();
    prevheights.resize(1);
    prevheights[0] = baseheight + 4;
    hash = tx.GetHash();
    mempool.addUnchecked(hash, entry.Time(GetTime()).FromTx(tx));
    BOOST_CHECK(!CheckFinalTx(tx, flags)); // Locktime fails
    BOOST_CHECK(TestSequenceLocks(tx, flags)); // Sequence locks pass
    BOOST_CHECK(IsFinalTx(tx, chainActive.Tip()->nHeight + 2, chainActive.Tip()->GetMedianTimePast() + 1)); // Locktime passes 1 second later

    // mempool-dependent transactions (not added)
    tx.vin[0].prevout.hash = hash;
    prevheights[0] = chainActive.Tip()->nHeight + 1;
    tx.nLockTime = 0;
    tx.vin[0].nSequence = 0;
    BOOST_CHECK(CheckFinalTx(tx, flags)); // Locktime passes
    BOOST_CHECK(TestSequenceLocks(tx, flags)); // Sequence locks pass
    tx.vin[0].nSequence = 1;
    BOOST_CHECK(!TestSequenceLocks(tx, flags)); // Sequence locks fail
    tx.vin[0].nSequence = CTxIn::SEQUENCE_LOCKTIME_TYPE_FLAG;
    BOOST_CHECK(TestSequenceLocks(tx, flags)); // Sequence locks pass
    tx.vin[0].nSequence = CTxIn::SEQUENCE_LOCKTIME_TYPE_FLAG | 1;
    BOOST_CHECK(!TestSequenceLocks(tx, flags)); // Sequence locks fail

    BOOST_CHECK(pblocktemplate = AssemblerForTest(chainparams).CreateNewBlock(scriptPubKey));

    // None of the of the absolute height/time locked tx should have made
    // it into the template because we still check IsFinalTx in CreateNewBlock,
    // but relative locked txs will if inconsistently added to mempool.
    // For now these will still generate a valid template until BIP68 soft fork
    BOOST_CHECK_EQUAL(pblocktemplate->block.vtx.size(), 3U);
    // However if we advance height by 1 and time by 512, all of them should be mined
    for (int i = 0; i < CBlockIndex::nMedianTimeSpan; i++)
        chainActive.Tip()->GetAncestor(chainActive.Tip()->nHeight - i)->nTime += 512; //Trick the MedianTimePast
    chainActive.Tip()->nHeight++;
    SetMockTime(chainActive.Tip()->GetMedianTimePast() + 1);

    BOOST_CHECK(pblocktemplate = AssemblerForTest(chainparams).CreateNewBlock(scriptPubKey));
    BOOST_CHECK_EQUAL(pblocktemplate->block.vtx.size(), 5U);

    chainActive.Tip()->nHeight--;
    SetMockTime(0);
    mempool.clear();

    TestPackageSelection(chainparams, scriptPubKey, txFirst);

    fCheckpointsEnabled = true;
}

BOOST_AUTO_TEST_SUITE_END()
