#ifndef QTUMVERSIONCHECKER_H
#define QTUMVERSIONCHECKER_H

#include <QObject>

#define CONCENSUM_DOWNLOAD_PAGE "https://concensum.org/network#wallet"
#define CONCENSUM_RELEASE_INFO "https://gitlab.com/concensum/concensum-core/snippets/1741305/raw"
#define CONCENSUM_RELEASE_INFO_LATEST_VERSION "latest_version"
#define CONCENSUM_RELEASE_INFO_RELEASE_URL "release_url"

class Version {

public:
    bool isNew() {
        return true;
    }

    int _major;
    int _minor;
    int _revision;
    QString _releaseUrl;

    Version(){
        SetNull();
    }

    Version(int maj, int min, int rev, const QString releaseUrl = QString()){
        SetNull();

        _major = maj;
        _minor = min;
        _revision = rev;
        _releaseUrl = releaseUrl;
    }

    Version(QString str, QString releaseUrl = QString()){
        SetNull();

        QStringList parts = str.split(".");

        if(!parts.isEmpty())
            _major = parts[0].toInt();
        if(parts.length() > 1)
            _minor = parts[1].toInt();
        if(parts.length() > 2)
            _revision = parts[2].toInt();

        _releaseUrl = releaseUrl;
    }

    Version(const Version &v){
        _major = v._major;
        _minor = v._minor;
        _revision = v._revision;
        _releaseUrl = v._releaseUrl;
    }

    bool operator >(const Version& other) const
    {
        return compareAll(other) > 0;
    }

    bool operator <(const Version& other) const
    {
        return compareAll(other) < 0;
    }

    bool operator ==(const Version& other) const
    {
        return compareAll(other) == 0;
    }

    void SetNull()
    {
        _major = 0;
        _minor = 0;
        _revision = 0;
        _releaseUrl = QString();
    }

    QString getVersion()
    {
        return QString("%1.%2.%3").arg(_major).arg(_minor).arg(_revision);
    }

    QString getReleaseUrl()
    {
        return _releaseUrl;
    }

private:
    int compare(int first, int second) const
    {
        int diff = first - second;
        return diff > 0 ? 1 : diff < 0 ? -1 : 0;
    }
    int compareAll(const Version& other) const
    {
        return 4 * compare(_major, other._major) +
               2 * compare(_minor, other._minor) +
               compare(_revision, other._revision);
    }
};

class QtumVersionChecker : public QObject
{
    Q_OBJECT
public:
    explicit QtumVersionChecker(QObject *parent = 0);
    ~QtumVersionChecker();

    Version getCurrentVersion();
    Version getLatestVersion();
    bool isVersionOutdated();
    QString getDownloadPage();

private:
    Version currentVersion;
};

#endif // QTUMVERSIONCHECKER_H
