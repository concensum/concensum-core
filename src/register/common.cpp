// Copyright (c) 2018 The Concensum Core developers
// Copyright (c) 2014-2017 Daniel Kraft
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <register/common.h>
#include <script/register.h>

bool fRecordHistory = false;

/* ************************************************************************** */
/* CRegisterData.  */

void CRegisterData::fromScript(const COutPoint& out, const CRegisterScript& script)
{
    value = script.getValue();
    prevout = out;
    addr = script.getAddress();
}

/* ************************************************************************** */
/* CRegisterIterator.  */

CRegisterIterator::~CRegisterIterator()
{
    /* Nothing to be done here.  This may be overwritten by
     subclasses if they need a destructor.  */
}

/* ************************************************************************** */
/* CCacheRegisterIterator.  */

class CCacheRegisterIterator : public CRegisterIterator
{
private:
    /** Reference to cache object that is used.  */
    const CRegisterCache& cache;

    /** Base iterator to combine with the cache.  */
    CRegisterIterator* base;

    /** Whether or not the base iterator has more entries.  */
    bool baseHasMore;

    /** "Next" registrar of the base iterator. */
    valtype baseRegistrar;
    /** "Next" record of the base iterator.  */
    valtype baseRecord;
    /** "Next" data of the base iterator.  */
    CRegisterData baseData;

    /** Iterator of the cache's entries.  */
    CRegisterCache::EntryMap::const_iterator cacheIter;

    std::map<valtype, CRegisterData>::const_iterator registrarIter;

    /* Call the base iterator's next() routine to fill in the internal
     "cache" for the next entry.  This already skips entries that are
     marked as deleted in the cache.  */
    void advanceBaseIterator();

public:
    /**
   * Construct the iterator.  This takes ownership of the base iterator.
   * @param c The cache object to use.
   * @param b The base iterator.
   */
    CCacheRegisterIterator(const CRegisterCache& c, CRegisterIterator* b);

    /* Destruct, this deletes also the base iterator.  */
    ~CCacheRegisterIterator();

    /* Implement iterator methods.  */
    void seek(const valtype& id);
    bool next(valtype& registrar, valtype& id, CRegisterData& data);
};

CCacheRegisterIterator::CCacheRegisterIterator(const CRegisterCache& c, CRegisterIterator* b)
    : cache(c), base(b)
{
    /* Add a seek-to-start to ensure that everything is consistent.  This call
     may be superfluous if we seek to another position afterwards anyway,
     but it should also not hurt too much.  */
    seek(valtype());
}

CCacheRegisterIterator::~CCacheRegisterIterator()
{
    delete base;
}

void CCacheRegisterIterator::advanceBaseIterator()
{
    assert(baseHasMore);
    do {
        baseHasMore = base->next(baseRegistrar, baseRecord, baseData);
    } while (baseHasMore && cache.isDeleted(baseRegistrar, baseRecord));
}

void CCacheRegisterIterator::seek(const valtype& start)
{
    cacheIter = cache.entries.lower_bound(start);
    base->seek(start);

    baseHasMore = true;
    advanceBaseIterator();
}

bool CCacheRegisterIterator::next(valtype& registrar, valtype& id, CRegisterData& data)
{
    /* Exit early if no more data is available in either the cache
     nor the base iterator.  */
    if (!baseHasMore && cacheIter == cache.entries.end()) {
        return false;
    }
    /* Determine which source to use for the next.  */
    bool useBase;
    if (!baseHasMore) {
        useBase = false;
    } else if (cacheIter == cache.entries.end()) {
        useBase = true;
    } else {
        /* A special case is when both iterators are equal.  In this case,
        we want to use the cached version.  We also have to advance
        the base iterator.  */
        if (baseRecord == cacheIter->first) {
            advanceBaseIterator();
        }
        /* Due to advancing the base iterator above, it may happen that
        no more base entries are present.  Handle this gracefully.  */
        if (!baseHasMore) {
            useBase = false;
        } else {
            assert(baseRecord != cacheIter->first);

            CRegisterCache::RegisterComparator cmp;
            useBase = cmp(baseRecord, cacheIter->first);
        }
    }

    /* Use the correct source now and advance it.  */
    if (useBase) {
        registrar = baseRegistrar;
        id = baseRecord;
        data = baseData;
        advanceBaseIterator();
    } else {
        registrar = cacheIter->first;
        //TODO this is not working properly needs to be rechecked
        std::map<valtype, CRegisterData>::const_iterator registerMap = cacheIter->second.find(registrar);
        if (registerMap != cacheIter->second.end()) {
            data = registerMap->second;
            ++cacheIter;
        }
    }
    return true;
}

/* ************************************************************************** */
/* CRegisterCache.  */

bool CRegisterCache::get(const valtype& registrar, const valtype& id, CRegisterData& data) const
{
    const EntryMap::const_iterator i = entries.find(registrar);
    if (i == entries.end()) {
        return false;
    } else {
        const std::map<valtype, CRegisterData>::const_iterator it = i->second.find(id);
        if (it == i->second.end()) {
            return false;
        } else {
            data = it->second;
        }
    }

    return true;
}

void CRegisterCache::set(const valtype& registrar, const valtype& id, const CRegisterData& data)
{
    const std::map<valtype, std::set<valtype>>::iterator di = deleted.find(registrar);

    if (di != deleted.end()) {
        const std::set<valtype>::iterator dit = di->second.find(id);
        if (dit != di->second.end()) {
            di->second.erase(dit);
        }
    }

    const EntryMap::iterator ei = entries.find(registrar);
    if (ei != entries.end()) {
        const std::map<valtype, CRegisterData>::iterator eri = ei->second.find(id);
        if (eri != ei->second.end()) {
            eri->second = data;
        } else {
            ei->second.insert(std::make_pair(id, data));
        }
    } else {
        std::map<valtype, CRegisterData, RegisterComparator> registrarRegisters;

        registrarRegisters.insert(std::make_pair(id, data));
        entries.insert(std::make_pair(registrar, registrarRegisters));
    }
}

void CRegisterCache::remove(const valtype& registrar, const valtype& id)
{
    const EntryMap::iterator ei = entries.find(registrar);
    if (ei != entries.end()) {
        const std::map<valtype, CRegisterData>::iterator it = ei->second.find(id);
        if (it != ei->second.end()) {
            ei->second.erase(it);
        }
    }

    std::map<valtype, std::set<valtype>>::iterator dit = deleted.find(registrar);
    if (dit != deleted.end()) {
        dit->second.insert(id);
    } else {
        std::set<valtype> deletedIds;
        deletedIds.insert(id);
        deleted.insert(std::make_pair(registrar, deletedIds));
    }
}

CRegisterIterator* CRegisterCache::iterateRegisters(CRegisterIterator* base) const
{
    return new CCacheRegisterIterator(*this, base);
}

void CRegisterCache::apply(const CRegisterCache& cache)
{
    for (EntryMap::const_iterator i = cache.entries.begin(); i != cache.entries.end(); ++i) {
        valtype registrar = i->first;

        for (std::map<valtype, CRegisterData>::const_iterator rit = i->second.begin(); rit != i->second.end(); ++rit) {
            set(registrar, rit->first, rit->second);
        }
    }

    for (std::map<valtype, std::set<valtype>>::const_iterator i = cache.deleted.begin(); i != cache.deleted.end(); ++i) {
        valtype registrar = i->first;
        for (std::set<valtype>::const_iterator dit = i->second.begin(); dit != i->second.end(); ++dit) {
            remove(registrar, *dit);
        }
    }
}
