// Copyright (c) 2018 The Concensum Core developers
// Copyright (c) 2014-2017 Daniel Kraft
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_REGISTER_COMMON_H
#define BITCOIN_REGISTER_COMMON_H

#include <compat/endian.h>
#include <primitives/transaction.h>
#include <script/script.h>
#include <serialize.h>

#include <map>
#include <set>

/***
 * Based on NameCoin Core implementation by Daniel Kraft
 */

class CRegisterScript;
class CDBBatch;

/** Whether or not record history is enabled.  */
extern bool fRecordHistory;

/* ************************************************************************** */
/* CRegisterData.  */

/**
 * Information stored for a record in the database.
 */
class CRegisterData
{
private:
    /** The records's value.  */
    valtype value;

    /** The record's last update outpoint.  */
    COutPoint prevout;

    /**
   * The records's address (as script).  This is kept here also, because
   * that information is useful to extract on demand (e. g., in listrecords).
   */
    CScript addr;

public:
    ADD_SERIALIZE_METHODS;

    template <typename Stream, typename Operation>
    inline void SerializationOp(Stream& s, Operation ser_action)
    {
        READWRITE(value);
        READWRITE(prevout);
        READWRITE(*(CScriptBase*)(&addr));
    }

    // TODO XXXXXXXX
    /* Compare for equality.  */
    friend inline bool
    operator==(const CRegisterData& a, const CRegisterData& b)
    {
        return a.value == b.value && a.prevout == b.prevout && a.addr == b.addr;
    }
    friend inline bool
    operator!=(const CRegisterData& a, const CRegisterData& b)
    {
        return !(a == b);
    }

    /**
   * Get the value.
   * @return The record's value.
   */
    inline const valtype&
    getValue() const
    {
        return value;
    }

    /**
   * Get the record's update outpoint.
   * @return The update outpoint.
   */
    inline const COutPoint&
    getUpdateOutpoint() const
    {
        return prevout;
    }

    /**
   * Get the address.
   * @return The records's address.
   */
    inline const CScript&
    getAddress() const
    {
        return addr;
    }

    /**
   * Set from a record update operation.
   * @param out The update outpoint.
   * @param script The register script.
   */
    void fromScript(const COutPoint& out, const CRegisterScript& script);
};

/* ************************************************************************** */
/* CRegisterIterator.  */

/**
 * Interface for iterators over the register database.
 */
class CRegisterIterator
{
public:
    // Virtual destructor in case subclasses need them.
    virtual ~CRegisterIterator();

    /**
   * Seek to a given lower bound.
   * @param start The record id to seek to.
   */
    virtual void seek(const valtype& id) = 0;

    /**
   * Get the next record.  Returns false if no more records are available.
   * @param id Put the id here.
   * @param data Put the records's data here.
   * @return True if successful, false if no more records.
   */
    virtual bool next(valtype& registrar, valtype& id, CRegisterData& data) = 0;
};

/* ************************************************************************** */
/* CRegisterCache.  */

/**
 * Cache / record of updates to the register database.  In addition to
 * new records (or updates to them), this also keeps track of deleted records
 * (when rolling back changes).
 */
class CRegisterCache
{
private:
    /**
   * Special comparator class for records that compares by length first.
   * This is used to sort the cache entry map in the same way as the
   * database is sorted.
   */
    class RegisterComparator
    {
    public:
        inline bool
        operator()(const valtype& a, const valtype& b) const
        {
            if (a.size() != b.size())
                return a.size() < b.size();

            return a < b;
        }
    };

public:
    /**
   * Type of register entry map.  This is public because it is also used
   * by the unit tests.
   */
    typedef std::map<valtype, std::map<valtype, CRegisterData, RegisterComparator>, RegisterComparator> EntryMap;

private:
    /** stored or updated records.  */
    EntryMap entries;
    /** deleted records.  */
    std::map<valtype, std::set<valtype>> deleted;

    friend class CCacheRegisterIterator;

public:
    inline void clear()
    {
        entries.clear();
        deleted.clear();
    }

    /**
   * Check if the cache is "clean" (no cached changes).  This also
   * performs internal checks and fails with an assertion if the
   * internal state is inconsistent.
   * @return True iff no changes are cached.
   */
    inline bool empty() const
    {
        if (entries.empty() && deleted.empty()) {
            return true;
        }

        return false;
    }

    /* See if the given record is marked as deleted.  */
    inline bool isDeleted(const valtype& registrar, const valtype& id) const
    {
        const std::map<valtype, std::set<valtype>>::const_iterator it = deleted.find(registrar);

        if (it != deleted.end()) {
            return it->second.count(id) > 0;
        }
        return false;
    }

    /* Try to get a records's associated data.  This looks only
     in entries, and doesn't care about deleted data.  */
    bool get(const valtype& registrar, const valtype& id, CRegisterData& data) const;

    /* Insert (or update) a record.  If it is marked as "deleted", this also
     removes the "deleted" mark.  */
    void set(const valtype& registrar, const valtype& id, const CRegisterData& data);

    /* Delete a record.  If it is in the "entries" set also, remove it there.  */
    void remove(const valtype& registrar, const valtype& id);

    /* Return a register iterator that combines a "base" iterator with the changes
     made to it according to the cache.  The base iterator is taken
     ownership of.  */
    CRegisterIterator* iterateRegisters(CRegisterIterator* base) const;

    /* Apply all the changes in the passed-in record on top of this one.  */
    void apply(const CRegisterCache& cache);

    /* Write all cached changes to a database batch update object.  */
    void writeBatch(CDBBatch& batch) const;
};

#endif // BITCOIN_REGISTER_COMMON_H
