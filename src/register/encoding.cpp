// Copyright (c) 2018 The Concensum Core developers
// Copyright (c) 2018 Daniel Kraft
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <register/encoding.h>

#include <util.h>
#include <utilstrencodings.h>

#include <univalue.h>
#include <key_io.h>
#include <script/standard.h>

#include <cassert>
#include <sstream>

RegisterEncoding EncodingFromString(const std::string& str) {
    if (str == "ascii") {
        return RegisterEncoding::ASCII;
    } 
    if (str == "utf8") {
        return RegisterEncoding::UTF8;
    }
    if (str == "hex") {
        return RegisterEncoding::HEX;
    }   
    throw std::invalid_argument ("invalid encoding: " + str);
}

std::string EncodingToString(RegisterEncoding enc) {
    switch (enc) {
        case RegisterEncoding::ASCII:
            return "ascii";
        case RegisterEncoding::UTF8:
            return "utf8";
        case RegisterEncoding::HEX:
            return "hex";
    }

  /* RegisterEncoding values are only ever created internally in the binary
     and not received externally (except as string).  Thus it should never
     happen (and if it does, is a severe bug) that we see an unexpected
     value.  */
  assert (false);
}

namespace
{

bool IsStringValid(const std::string& str, const RegisterEncoding enc)
{
  switch (enc)
    {
    case RegisterEncoding::ASCII:
      for (const unsigned char c : str)
        if (c < 0x20 || c >= 0x80)
          return false;
      return true;

    case RegisterEncoding::UTF8:
      if (!IsValidUtf8String(str))
        return false;
      return true;

    case RegisterEncoding::HEX:
      return str == "" || IsHex (str);
    }

  assert (false);
}

void VerifyStringValid (const std::string& str, const RegisterEncoding enc) {
  if (IsStringValid (str, enc))
    return;

  throw InvalidRegisterString (enc, str);
}

std::string
InvalidRegisterStringMessage (const RegisterEncoding enc, const std::string& invalidStr)
{
  std::ostringstream msg;
  msg << "invalid string for encoding " << EncodingToString (enc) << ":"
      << " " << invalidStr;
  return msg.str ();
}

} // anonymous namespace

InvalidRegisterString::InvalidRegisterString (const RegisterEncoding enc,
                                      const std::string& invalidStr)
  : std::invalid_argument (InvalidRegisterStringMessage (enc, invalidStr))
{}

std::string valtype2String(const valtype& data, const RegisterEncoding enc) {
  
  std::string res;
  switch (enc)
    {
    case RegisterEncoding::ASCII:
    case RegisterEncoding::UTF8:
      res = std::string (data.begin (), data.end ());
      break;

    case RegisterEncoding::HEX:
      res = HexStr (data.begin (), data.end ());
      break;
    }

  VerifyStringValid (res, enc);
  return res;
}

valtype string2Valtype(const std::string& str, const RegisterEncoding enc) {
  
  VerifyStringValid (str, enc);
  switch (enc)
    {
    case RegisterEncoding::ASCII:
    case RegisterEncoding::UTF8:
      return valtype (str.begin (), str.end ());

    case RegisterEncoding::HEX:
      return ParseHex (str);
    }

  assert (false);
}

valtype destination2Valtype (const CTxDestination dest) {

  // TODO
  // if we assume CKeyID for the purpose we are using this method
  // we must contain its usage, otherwise we should handle
  // the other CTxDestination types with a visitor
  CKeyID destKeyID = boost::get<CKeyID>(dest);
  return valtype(destKeyID.begin(), destKeyID.end());
}