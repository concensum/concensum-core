// Copytight (c) 2018 The Concensum Core developers
// Copyright (c) 2018 Daniel Kraft
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_REGISTER_ENCODING_H
#define BITCOIN_REGISTER_ENCODING_H

#include <script/script.h>

#include <script/standard.h>

#include <stdexcept>
#include <string>

class UniValue;

/**
 * Enum for the possible encodings
 */
enum class RegisterEncoding
{
  /**
   * Only printable ASCII characters (code in [0x20, 0x80)) are allowed.
   */
  ASCII,

  /**
   * Valid UTF-8 with printable characters (code >=0x20).
   */
  UTF8,

  /**
   * Hex-encoded arbitrary binary data.
   */
  HEX,
};

/* Utility functions to convert encodings to/from the enum.  They throw
   std::invalid_argument if the conversion fails.  */
RegisterEncoding EncodingFromString (const std::string& str);
std::string EncodingToString (RegisterEncoding enc);

/**
 * Exception that is thrown if a string is invalid according
 * to the chosen encoding.
 */
class InvalidRegisterString : public std::invalid_argument
{

public:

  InvalidRegisterString () = delete;

  InvalidRegisterString (RegisterEncoding enc, const std::string& invalidStr);

};

/**
 * Encodes valtype to string with the given encoding.  Throws
 * InvalidString if the data is not valid for the encoding.
 */
std::string valtype2String (const valtype& data, RegisterEncoding enc = RegisterEncoding::UTF8);

/**
 * Decodes a string to a valtype. Throws InvalidString
 * if the string is invalid for the requested encoding.
 */
valtype string2Valtype (const std::string& str, RegisterEncoding enc = RegisterEncoding::UTF8);

/**
 * Encodes CTXDestination as a valtype
 */
valtype destination2Valtype (const CTxDestination dest);

#endif // BITCOIN_REGISTER_ENCODING_H
