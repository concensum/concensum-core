// Copyright (c) 2018 The Concensum Core developers
// Copyright (c) 2014-2017 Daniel Kraft
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <register/main.h>

#include <register/encoding.h>



#include <consensus/validation.h>
#include <pubkey.h>
#include <txmempool.h>
#include <undo.h>
#include <validation.h>

/* ************************************************************************** */
/* CRegisterTxUndo.  */

void CRegisterTxUndo::fromOldState(const valtype& address, const valtype& nm, const CCoinsView& view)
{
    registrar = address;
    id = nm;
    isNew = !view.GetRecord(registrar, id, oldData);
}

void CRegisterTxUndo::apply(CCoinsViewCache& view) const
{
    if (isNew) {
        view.DeleteRecord(registrar, id);
    } else {
        view.SetRecord(registrar, id, oldData);
    }
}

void CRegisterMemPool::addUnchecked(const CTxMemPoolEntry& entry)
{
    AssertLockHeld(pool.cs);

    if (entry.isRegisterStoreOrUpdate()) {
        const CRegisterScript regScript = entry.getRegisterScript();

        CTxDestination destination;
        if (ExtractDestination(regScript.getAddress(), destination)) {

            const valtype registrar = destination2Valtype(destination);
            const RegisterTxMap::iterator it = mapKeyRegs.find(registrar);

            const valtype& id = entry.getRegisterId();
            const uint256& txHash = entry.GetTx().GetHash();

            if (it == mapKeyRegs.end()) {
                std::map<valtype, uint256> registerData;
                registerData.insert(std::make_pair(id, txHash));
                mapKeyRegs.insert(std::make_pair(registrar, registerData));
            } else {
                // should not have duplicate ids for same address
                assert(it->second.count(id) == 0);
                it->second.insert(std::make_pair(id, txHash));
            }
        }
    }
}

void CRegisterMemPool::remove(const CTxMemPoolEntry& entry)
{
    AssertLockHeld(pool.cs);

    if (entry.isRegisterStoreOrUpdate()) {
        CTxDestination destination;
        if (ExtractDestination(entry.getRegisterScript().getAddress(), destination)) {
            const valtype registrar = destination2Valtype(destination);
            const RegisterTxMap::iterator mit = mapKeyRegs.find(registrar);
            if (mit != mapKeyRegs.end()) {
                const std::map<valtype, uint256>::const_iterator rit = mit->second.find(entry.getRegisterId());
                assert(rit != mit->second.end()); // is not empty
                mit->second.erase(rit);
            }
        }
    }
}

void CRegisterMemPool::removeConflicts(const CTransaction& tx)
{
    AssertLockHeld(pool.cs);

    if (!tx.IsRegisterTx())
        return;

    for (const auto& txout : tx.vout) {
        const CRegisterScript registerOp(txout.scriptPubKey);
        if (registerOp.isRegisterOp()) {
            CTxDestination destination;
            if (ExtractDestination(registerOp.getAddress(), destination)) {
                const valtype registrar = destination2Valtype(destination);
                const RegisterTxMap::const_iterator mit = mapKeyRegs.find(registrar);
                if (mit != mapKeyRegs.end()) {
                    const std::map<valtype, uint256>::const_iterator rit = mit->second.find(registerOp.getId());

                    if (rit != mit->second.end()) {
                        const CTxMemPool::txiter mit2 = pool.mapTx.find(rit->second);
                        assert(mit2 != pool.mapTx.end());
                        pool.removeRecursive(mit2->GetTx(), MemPoolRemovalReason::REGISTER_CONFLICT);
                    }
                }
            }
        }
    }
}

void CRegisterMemPool::check(const CCoinsView& coins) const
{
    AssertLockHeld(pool.cs);

    std::map<valtype, std::set<valtype>> records;
    int nrEntries(0);
    for (const auto& entry : pool.mapTx) {
        if (entry.isRegisterStoreOrUpdate()) {
            CTxDestination destination;
            if (ExtractDestination(entry.getRegisterScript().getAddress(), destination)) {
                const valtype registrar = destination2Valtype(destination);
                const RegisterTxMap::const_iterator mit = mapKeyRegs.find(registrar);
                assert(mit != mapKeyRegs.end());

                const valtype& id = entry.getRegisterId();
                const uint256 txHash = entry.GetTx().GetHash();
                const std::map<valtype, uint256>::const_iterator rit = mit->second.find(id);
                assert(rit->second == txHash);

                assert(records.count(id) == 0);

                const std::map<valtype, std::set<valtype>>::iterator sit = records.find(registrar);
                if(sit != records.end()){
                    //if there is already an entry for the registrar, the id shouldn't be there
                    assert(sit->second.count(id) == 0);
                    sit->second.insert(id);
                    nrEntries++;
                } else {
                    std::set<valtype> idSet;
                    idSet.insert(id);
                    records.insert(std::make_pair(registrar, idSet));
                    nrEntries++;
                }
            }
        }
    }
    assert(nrEntries == getNrTransactions());
}

bool CRegisterMemPool::checkTx(const CTransaction& tx) const
{
    // we cannot accept on the mempool duplicated storerecord txs
    // if we do it we'll get a stuck record forever

    AssertLockHeld(pool.cs);

    // we cannot distinguish at this point if is an update or store
    // so for now we only accept a single register/update operation
    // per <registrar,id> at the mempool
    // we only want possible storerecords to be checked

    for (const auto& txout : tx.vout) {

        const CRegisterScript registerOp(txout.scriptPubKey);

        // if there are no op register outs it will pass anyway
        if (!registerOp.isRegisterOp()) {
            continue;
        }
        else {

            CTxDestination destination;
            if (ExtractDestination(registerOp.getAddress(), destination)) {
                const valtype registrar = destination2Valtype(destination);
                const valtype& id = registerOp.getId();
                if (storeRecordExists(registrar, id)) {
                    return false;
                }
            } else {
                // if we cannot extract destination from, something is wrong
                return false;
            }
            // assuming we only have op_register output per tx
            break;
        }
    }
    return true;
}

int CRegisterMemPool::getNrTransactions() const
{
    int nrTransactions = 0;
    for (RegisterTxMap::const_iterator iter = mapKeyRegs.begin(); iter != mapKeyRegs.end(); ++iter) {
        nrTransactions += iter->second.size();
    }

    return nrTransactions;
}

bool CheckRegisterTransaction(const CTransaction& tx, const CCoinsView& view, CValidationState& state)
{
    const std::string strTxid = tx.GetHash().GetHex();
    const char* txid = strTxid.c_str();

    int registerOut = -1;
    CRegisterScript registerScriptOut;
    for (unsigned i = 0; i < tx.vout.size(); ++i) {
        const CRegisterScript script(tx.vout[i].scriptPubKey);
        if (script.isRegisterOp()) {
            if (registerOut != -1) {
                return state.Invalid(
                    error("%s: Tx %s: multiple register outputs", __func__, txid));
            }
            registerOut = i;
            registerScriptOut = script;
        }
    }

    CTxDestination registerDestination;
    if (registerOut != -1 && !ExtractDestination(registerScriptOut.getRawScript(), registerDestination)) {
        return state.Invalid(
            error("%s: Tx %s: invalid register script", __func__, txid));
    }

    int registerIn = -1;
    CRegisterScript registerScriptIn;
    CAmount amountRemaining = REGISTER_REQUIRED_AMOUNT;
    bool fAddressFound = false;
    for (unsigned i = 0; i < tx.vin.size(); ++i) {
        const COutPoint& prevout = tx.vin[i].prevout;
        Coin coin;
        if (!view.GetCoin(prevout, coin)) {
            if (tx.IsCoinBase()) {
                continue;
            }
            return state.DoS(100, error("%s: failed to fetch input coin for %s", __func__, txid),
                REJECT_INVALID, "bad-txns-inputs-missingorspent");
        }

        const CRegisterScript script(coin.out.scriptPubKey);
        if (script.isRegisterOp()) {
            if (registerIn != -1) {
                return state.Invalid(
                    error("%s: Tx %s: multiple register inputs", __func__, txid));
            }
            registerIn = i;
            registerScriptIn = script;
        }

        if (!fAddressFound && registerIn == -1) {
            CTxDestination address;
            if (ExtractDestination(coin.out.scriptPubKey, address)) {
                if (registerDestination == address) {
                    fAddressFound = true;
                }
            }
        }

        amountRemaining -= coin.out.nValue;
    }

    // no register inputs or outputs
    if (registerIn == -1 && registerOut == -1) {
        return true;
    }

    if (registerIn != -1) {
        // vin has register input -> is an update or delete
        if (registerOut == -1) {
            // its a DELETE RECORD
            // we don't allow deletions
            // return state.Invalid(
            //     error("%s: Tx %s has register input but no register output", __func__, txid));
            // or do we :)
        } else {
            // its an UPDATE RECORD
            CTxDestination addressIn;
            bool fSameAddress =
                ExtractDestination(registerScriptIn.getRawScript(), addressIn) && addressIn == registerDestination;

            bool fSameKey = registerScriptIn.getId() == registerScriptOut.getId();

            if (!fSameAddress || !fSameKey) {
                return state.Invalid(
                    error("%s: Tx %s: id and/or registrar mismatch for register update", __func__, txid));
            }
        }
    } else {

        // no register input -> is a STORE RECORD
        // => sum(vin[].nValue > 10K) && vin.address == registerDest

        if (!fAddressFound) {
            return state.Invalid(
                error("%s: Tx %s missing input from registrar address", __func__, txid));
        }

        if (amountRemaining > 0) {
            return state.Invalid(
                error("%s: Tx %s has register output but not enough input amount", __func__, txid));
        }

        valtype registrar = destination2Valtype(registerDestination);
        CRegisterData existingData;
        if (view.GetRecord(registrar, registerScriptOut.getId(), existingData)) {
            return state.Invalid(error("%s: Tx %s: record already exists", __func__, txid));
        }
    }

    if (registerOut != -1) {
        if (registerScriptOut.getId().size() > MAX_ID_LENGTH) {
            return state.Invalid(error("%s: Tx %s: record id is too long", __func__, txid));
        }
        const valtype& value = registerScriptOut.getValue();
        if (registerScriptOut.getValue().size() > MAX_VALUE_LENGTH) {
            return state.Invalid(error("%s: Tx %s: record value is too long", __func__, txid));
        }
    }

    return true;
}

void ApplyRegisterTransaction(const CTransaction& tx, unsigned nHeight, CCoinsViewCache& view, CBlockUndo& undo)
{
    // TODO remvoe nHeight

    const std::string strTxid = tx.GetHash().GetHex();
    const char* txid = strTxid.c_str();

    const bool fIsCoinBase = tx.IsCoinBase();

    assert(nHeight != MEMPOOL_HEIGHT);

    /* Changes are encoded in the outputs.  We don't have to do any checks,
    so simply apply all these.  */

    CRegisterScript registerScriptIn;
    for (unsigned i = 0; i < tx.vin.size(); ++i) {

        const COutPoint& prevout = tx.vin[i].prevout;
        Coin coin;
        if (!view.GetCoin(prevout, coin)) {
            if (!fIsCoinBase) {
                error("%s: failed to fetch input coin for %s", __func__, txid);
            }
            // this continue is extremely IMPORTANT
            // Apply cannot reject a transaction at this point so we need
            // to apply changes in register because the tx will
            // be persisted anyway...
            continue;
        }

        const CRegisterScript script(coin.out.scriptPubKey);
        if (script.isRegisterOp()) {
            registerScriptIn = script;
            // only 1 op_register input exist at this point
            break;
        }
    }

    int registerOut = -1;
    CRegisterScript registerScriptOut;
    for (unsigned i = 0; i < tx.vout.size(); ++i) {
        const CRegisterScript op(tx.vout[i].scriptPubKey);
        if (op.isRegisterOp()) {
            registerOut = i;
            registerScriptOut = op;
            // only 1 op_register output exist max so we can just skip the rest
            break;
        }
    }

    if (registerScriptIn.isRegisterOp() && !registerScriptOut.isRegisterOp()) {

        // delete

        CTxDestination destination;
        if (!ExtractDestination(registerScriptIn.getAddress(), destination)) {
            error("%s: failed to extract destination from %s", __func__, txid);
        }

        const valtype registrar = destination2Valtype(destination);
        const valtype& id = registerScriptIn.getId();

        LogPrintf("Deleting record at height %d: %s\n", nHeight, valtype2String(id));

        CRegisterTxUndo opUndo;
        opUndo.fromOldState(registrar, id, view);
        undo.vregisterundo.push_back(opUndo);

        view.DeleteRecord(registrar, id);
    }
    else if (registerScriptOut.isRegisterOp()) {

        // store or update

        CTxDestination destination;
        if (!ExtractDestination(registerScriptOut.getAddress(), destination)) {
            error("%s: failed to extract destination from %s", __func__, txid);
        }

        const valtype registrar = destination2Valtype(destination);
        const valtype& id = registerScriptOut.getId();

        if (registerScriptIn.isRegisterOp()) {
            // update
            LogPrintf("Updating record at height %d: %s\n", nHeight, valtype2String(id));
        }
        else {
            // store
            LogPrintf("Storing record at height %d: %s\n", nHeight, valtype2String(id));
        }

        CRegisterTxUndo opUndo;
        opUndo.fromOldState(registrar, id, view);
        undo.vregisterundo.push_back(opUndo);

        CRegisterData data;
        data.fromScript(COutPoint(tx.GetHash(), registerOut), registerScriptOut);
        view.SetRecord(registrar, id, data);
    }
}
