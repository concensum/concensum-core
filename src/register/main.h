// Copyright (c) 2018 The Concensum Core developers
// Copyright (c) 2014-2017 Daniel Kraft
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_REGISTER_MAIN_H
#define BITCOIN_REGISTER_MAIN_H

#include <register/common.h>

class CBlockUndo;
class CCoinsView;
class CCoinsViewCache;
class CTxMemPool;
class CTxMemPoolEntry;
class CValidationState;

/* Some constants defining register limits.  */
static const unsigned MAX_ID_LENGTH = 255;
static const unsigned MAX_VALUE_LENGTH = 1023;
static const unsigned MAX_VALUE_LENGTH_UI = 520;

static const CAmount REGISTER_REQUIRED_AMOUNT = 10000 * COIN; // 10k

/* ************************************************************************** */
/* CRegisterTxUndo.  */

/**
 * Undo information for one register operation.  This contains either the
 * information that the record was newly created (and should thus be
 * deleted entirely) or that it was updated including the old value.
 */
class CRegisterTxUndo
{
private:

    /** The registrar address that contains this id **/
    valtype registrar;

    /** The id this concerns.  */
    valtype id;

    /** Whether this was an entirely new record (no update).  */
    bool isNew;

    /** The old record value that was overwritten by the operation.  */
    CRegisterData oldData;

public:
    ADD_SERIALIZE_METHODS;

    template <typename Stream, typename Operation>
    inline void SerializationOp(Stream& s, Operation ser_action)
    {
        READWRITE(registrar);
        READWRITE(id);
        READWRITE(isNew);
        if (!isNew)
            READWRITE(oldData);
    }

    /**
   * Set the data for an update/registration of the given name. The CCoinsView
   * is used to find out all the necessary information.
   * @param address The address on where the record is being updated
   * @param nm The record that is being updated.
   * @param view The (old!) chain state.
   */
    void fromOldState(const valtype& address, const valtype& nm, const CCoinsView& view);

    /**
   * Apply the undo to the chain state given.
   * @param view The chain state to update ("undo").
   */
    void apply(CCoinsViewCache& view) const;
};

/* ************************************************************************** 
* CRegisterMemPool.
*
* (Based on CNameMemPool from Daniel Kraft (NameCoin)
*
*/

/**
 * Handle the register component of the transaction mempool.  This keeps track
 * of register operations that are in the mempool and ensures that all transactions
 * kept are consistent.  E. g., no two transactions are allowed to register
 * the same resource for the same issuer, and resource registration transactions are removed if a
 * conflicting registration makes it into a block.
 */
class CRegisterMemPool
{
private:
    /** The parent mempool object.  Used to, e. g., remove conflicting tx.  */
    CTxMemPool& pool;

    /** Type used for internal indices.  */
    typedef std::map<valtype, std::map<valtype, uint256>> RegisterTxMap;

    /**
   * Keep track of keys that are registered by transactions in the pool.
   * Map ids to registering transaction.
   */
    RegisterTxMap mapKeyRegs;

public:
    /**
   * Construct with reference to parent mempool.
   * @param p The parent pool.
   */
    explicit inline CRegisterMemPool(CTxMemPool& p)
        : pool(p), mapKeyRegs()
    {
    }

    /**
   * Check whether a particular resource is being registered by
   * some transaction in the mempool.  Does not lock, this is
   * done by the parent mempool (which calls through afterwards).
   * @param registrar The registrar address on where to check for the key
   * @param key The key to check for.
   * @return True iff there's a matching key registration in the pool.
   */
    inline bool storeRecordExists(const valtype& registrar, const valtype& key) const
    {
        const RegisterTxMap::const_iterator it = mapKeyRegs.find(registrar);

        if (it != mapKeyRegs.end()) {
            return it->second.count(key) > 0;
        }

        return false;
    }

    /**
   * Clear all data.
   */
    inline void clear()
    {
        mapKeyRegs.clear();
    }

    /**
   * Add an entry without checking it.  It should have been checked
   * already.  If this conflicts with the mempool, it may throw.
   */
    void addUnchecked(const CTxMemPoolEntry& entry);

    /**
   * Remove the given mempool entry.  It is assumed that it is present.
   * @param entry The entry to remove.
   */
    void remove(const CTxMemPoolEntry& entry);

    /**
   * Remove conflicts for the given tx, based on register operations.  I. e.,
   * if the tx registers a key that conflicts with another registration
   * in the mempool, detect this and remove the mempool tx accordingly.
   * @param tx The transaction for which we look for conflicts.
   * @param removed Put removed tx here.
   */
    void removeConflicts(const CTransaction& tx);

    /**
   * Perform sanity checks.  Throws if it fails.
   * @param coins The coins view this represents.
   */
    void check(const CCoinsView& coins) const;

    /**
   * Check if a tx can be added (based on key criteria) without
   * causing a conflict.
   * @param tx The transaction to check.
   * @return True if it doesn't conflict.
   */
    bool checkTx(const CTransaction& tx) const;

    /**
     * Gets the number of transactions on the RegisterMemPool
     */
    int getNrTransactions() const;
};

/* ************************************************************************** */

/**
 * Check a transaction according to the additional Register rules.
 * @param tx The transaction to check.
 * @param view The current chain state.
 * @param state Resulting validation state.
 * @return True in case of success.
 */
bool CheckRegisterTransaction(const CTransaction& tx, const CCoinsView& view, CValidationState& state);

/**
 * Apply the changes of a register transaction to the register database.
 * @param tx The transaction to apply.
 * @param nHeight Height at which the tx is. Used for CRegisterData.
 * @param view The chain state to update.
 * @param undo Record undo information here.
 */
void ApplyRegisterTransaction(const CTransaction& tx, unsigned nHeight, CCoinsViewCache& view, CBlockUndo& undo);

/**
 * Check the register database consistency.  This calls CCoinsView::ValidateRegisterDB,
 * but only if applicable depending on the -checkregisterdb setting.  If it fails,
 * this throws an assertion failure.
 * @param disconnect Whether we are disconnecting blocks.
 */
void CheckRegisterDB(bool disconnect);


#endif // BITCOIN_REGISTER_MAIN_H