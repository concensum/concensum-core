// Copyright (c) 2008 The Concensum Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_SCRIPT_REGISTER_H
#define BITCOIN_SCRIPT_REGISTER_H

#include <script/script.h>

class CRegisterScript
{

private:

    opcodetype op;

    // the operation arguments
    std::vector<valtype> args;

    // The non-register part, i. e., the address
    CScript address; // make this a CTXDestination

    CScript rawScript;

public:

    /**
     * Default constructor.  This enables us to declare a variable
     * and initialise it later via assignment.
     */
    inline CRegisterScript() : 
        op(OP_NOP) 
    {}

    /**
     * Parse a script and determine whether it is a valid register script.  Sets
     * the member variables representing the "picked apart" register script.
     * @param script The ordinary script to parse.
     */
    explicit CRegisterScript(const CScript& script);

    inline bool isRegisterOp() const 
    {
        return OP_REGISTER == op;
    }

    inline const valtype& getVersion() const
    {
        return args[2];
    }

    inline const valtype& getId() const
    {
        return args[0];
    }

    inline const valtype& getValue() const
    {
        return args[1];
    }

    inline const CScript& getAddress() const
    {
        return address;
    }

    inline const CScript& getRawScript() const
    {
        return rawScript;
    }

    static CScript buildStoreRecord(const CScript& addr, const valtype& id, const valtype& value);
};

#endif // BITCOIN_SCRIPT_REGISTER_H
