// Copyright (c) 2008 The Concensum Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <script/register.h>

namespace {

static const int VERSION = 1;

const opcodetype AsmForVersion(int v)
{
    switch (v)
    {
        case 1: return OP_1;
    }
}

} // namespace

CRegisterScript::CRegisterScript(const CScript& script)
    : op(OP_NOP), address(script), rawScript(script)
{
    CScript::const_iterator pc = script.begin();

    opcodetype opcode;
    for (int i = 0; i < 3; ++i) { // expected 3 data arguments, version, id and value

        valtype vch;
        if (!script.GetOp (pc, opcode, vch)) {
            return;
        }

        // id, value
        if (i <= 1 && 0 <= opcode && opcode < OP_PUSHDATA4) {
            args.push_back(vch);
            continue;
        }
        // version
        else if (i == 2 && OP_1 <=  opcode && opcode <= OP_16) {
            args.push_back(vch);
            continue;
        }

        return;
    }

    opcodetype registerOp;
    if (!script.GetOp (pc, registerOp) || registerOp != OP_REGISTER) { // expected OP_REGISTER
        return;
    }

    op = registerOp;
    address = CScript (pc, script.end());
    return;
}

CScript CRegisterScript::buildStoreRecord(const CScript& addr, const valtype& id, const valtype& value)
{
  CScript prefix;
  prefix << id << value << AsmForVersion(VERSION) << OP_REGISTER;

  return prefix + addr;
}