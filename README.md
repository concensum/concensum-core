What is Concensum?
-------------

[Concensum](https://concensum.org) is a Proof of Stake blockchain, based on [Qtum](https://qtum.org).

It aims to be a global storage for the copyright register of digital content.

Its built on Bitcoin's UTXO model. Most of Qtum features were inherited by default. The support for smart contracts and the EVM is intentionally disabled for now.

The Concensum network currency is the **CEN**.

For more information visit https://concensum.org.

What is Concensum Core?
------------------

Concensum Core is our primary mainnet wallet. It implements a full node and is capable of storing, validating, and distributing all history of the Concensum network. Concensum Core is considered the reference implementation for the Concensum network. 

Concensum Core currently implements the following:

* Sending/Receiving CEN
* Staking and creating blocks for the Concensum network
* Running a full node for distributing the blockchain to other users
* "Prune" mode, which minimizes disk usage
* Compatibility with the Bitcoin Core set of RPC commands and APIs

Concensum Core Download
--------

You can get compiled binaries from Concensum official website: https://concensum.org/en/network#wallets-explorer.

or you can look at the latest release at https://gitlab.com/concensum/concensum-core/tags for more options.

Building Concensum Core
----------

### On Ubuntu

    # This is a quick start script for compiling Concensum Core on Ubuntu

    sudo apt-get install build-essential libtool autotools-dev automake pkg-config libssl-dev libevent-dev bsdmainutils git cmake libboost-all-dev
    sudo apt-get install software-properties-common
    sudo add-apt-repository ppa:bitcoin/bitcoin
    sudo apt-get update
    sudo apt-get install libdb4.8-dev libdb4.8++-dev

    # If you want to build the Qt GUI:
    sudo apt-get install libqt5gui5 libqt5core5a libqt5dbus5 qttools5-dev qttools5-dev-tools libprotobuf-dev protobuf-compiler qrencode

    git clone https://gitlab.com/concensum/concensum-core --recursive
    cd concensum-core

    # Note autogen will prompt to install some more dependencies if needed
    ./autogen.sh
    ./configure 
    make -j2

Running Concensum Core
----------

The build process results in 4 different binaries `src/concensumd`, `src/concensum-cli`, `src/concensum-tx` and `src/qt/concensum-qt`.

Most of the common users will prefer to run the `src/qt/concensum-qt`, which is the Qt Gui and therefore a more user friendly interface.

For staking it is recommended to use tha daemon. You can run the command-line daemon using `concensumd` which you can interact with using the `concensum-cli`.
